-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2014 at 04:55 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_db`
--

CREATE TABLE IF NOT EXISTS `access_db` (
`id` int(10) unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `eroute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iroutes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parentid` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `access_db`
--

INSERT INTO `access_db` (`id`, `role`, `eroute`, `iroutes`, `name`, `parentid`) VALUES
(11, 'UL1', '/upldpayslip', '', 'Upload Payslips', 0),
(12, 'UL1', '/vieweclient', '', 'Create Client', 0),
(13, 'CL1', '/viewepayslip', '', 'View Payslip', 0),
(14, 'CL2', '/viewepayslip', '', 'View Payslip', 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_db`
--

CREATE TABLE IF NOT EXISTS `company_db` (
`id` int(11) NOT NULL,
  `coname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_db`
--

INSERT INTO `company_db` (`id`, `coname`) VALUES
(1, '5 Star Cosmetics Limited');

-- --------------------------------------------------------

--
-- Table structure for table `employee_db`
--

CREATE TABLE IF NOT EXISTS `employee_db` (
`id` int(11) NOT NULL,
  `emp_ref` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `ninumber` varchar(50) NOT NULL,
  `coname` varchar(100) NOT NULL,
  `firstpay` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `employee_db`
--

INSERT INTO `employee_db` (`id`, `emp_ref`, `name`, `address`, `ninumber`, `coname`, `firstpay`) VALUES
(1, '14', 'Miss. CE Clarke', '', 'PW955030C', '5 Star Cosmetics Limited', '1223.87'),
(2, '1', 'Miss. L Speller', '', 'NE698935A', '5 Star Cosmetics Limited', '687.69'),
(3, '5', 'Mr. L BAGGA', '', 'NR977660C', '5 Star Cosmetics Limited', '944.09'),
(4, '7', 'Mrs. T TOWERS', '', 'NX313057A', '5 Star Cosmetics Limited', '1130.86'),
(5, '9', 'Mr. C ORD', '', 'WK908532B', '5 Star Cosmetics Limited', '1326.56'),
(6, '12', 'Miss. R Piff', '', 'JZ724473B', '5 Star Cosmetics Limited', '1000.57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_09_13_113214_create_user_table', 1),
('2014_09_13_113326_create_session_table', 1),
('2014_09_14_204743_create_access_table', 1),
('2014_09_16_195026_create_sessiondb_table', 1),
('2014_12_09_091137_create_password_reminders_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_reminders`
--

INSERT INTO `password_reminders` (`email`, `token`, `created_at`) VALUES
('vishal@corientbs.com', '7a93826ed5c48f413917920cc7e25ad5668c6cc5', '2014-12-09 03:49:35'),
('vishal@corientbs.com', 'f20ffe534ac0ec0b3d75a4f10e760725a1b09d35', '2014-12-09 03:51:59'),
('vishal@corientbs.com', '46b00182c2a2ec5b103779a2366df256bb95ed32', '2014-12-09 03:52:07'),
('vishal@corientbs.com', '71fd0a4971e53e679ed68650e0b5267870b3fda9', '2014-12-09 03:53:54'),
('vishal@corientbs.com', 'e387ab02449fd0a4ce8ae6303657c0c5b5a09355', '2014-12-09 03:53:59'),
('vishal@corientbs.com', '3a81dfd5f4286c5c5d25c86833fafd168696bde1', '2014-12-09 03:55:42'),
('vishal@corientbs.com', '9f2d4e3a183f3294d6e720d9db7d5446ffbf8eb4', '2014-12-09 05:10:36'),
('vishal@corientbs.com', '26850796fe2208f6fe1be9bec1ae9bf6d7d0a92f', '2014-12-09 05:10:45'),
('vishal@corientbs.com', 'd2ca8b75cbebc5fc356630e4e72b9a010fb0c0af', '2014-12-09 05:13:24'),
('vishal@corientbs.com', 'd3879a551a9287b72fc135e8ba4ba39ff8326b69', '2014-12-09 05:14:15'),
('vishal@corientbs.com', '282bccacd4133c51857cd3da6df5d70d4305ece0', '2014-12-09 05:15:08'),
('vishal@corientbs.com', 'fd12301e611c2f28c7363381b95199620d331d23', '2014-12-09 05:16:07'),
('vishal@corientbs.com', '54ef3b6bb1888bd1523eba4fe9e217a97d2fc113', '2014-12-09 05:16:39'),
('vishal@corientbs.com', '971cd19cb65b22918744c4c446229f188495a62a', '2014-12-09 05:18:12'),
('vishal@corientbs.com', '517992d34784707a2d7d2643883c6b4bd8db3da0', '2014-12-09 05:19:42'),
('vishal@corientbs.com', '6216893bc733d75f36fa1c0776b32202f57532cf', '2014-12-09 05:21:40'),
('vishal@corientbs.com', '8257c40d09007ff9b22917bce9fd0b1695024344', '2014-12-09 05:23:10'),
('vishal@corientbs.com', 'ff99ae589fd29f7799f49ef1adb85472fde56c42', '2014-12-09 05:24:37'),
('vishal@corientbs.com', 'e077b7b66315a8f0d7c81fd391932626145cf572', '2014-12-09 05:24:59'),
('vishal@corientbs.com', 'c11e30dfc54001b27f43a38bebaf31ce7233cc57', '2014-12-09 05:25:26'),
('vishal@corientbs.com', '05d30735a62754d79d416938b8ce38cade5c0f68', '2014-12-09 05:26:37'),
('vishal@corientbs.com', 'bf4b94c9ea0289202a575fb27ddb2da0d73130fc', '2014-12-09 05:29:30'),
('vishal@corientbs.com', '3c0d30d3f0f97c6fdb658a017d0b750f05cc6038', '2014-12-09 06:08:04'),
('vishal@corientbs.com', '7a5c785741c2e6ccdb01a27cb387aa21b32a0c39', '2014-12-09 06:11:31'),
('vishal@corientbs.com', '02abacd0a033d5507a2d828c44810ddbc0cd70b6', '2014-12-09 08:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `payslip_db`
--

CREATE TABLE IF NOT EXISTS `payslip_db` (
`id` int(11) NOT NULL,
  `Emp_id` varchar(20) NOT NULL,
  `Co_Name` varchar(50) NOT NULL,
  `Payslip_Date` varchar(50) NOT NULL,
  `Gross_Pay` varchar(20) DEFAULT NULL,
  `Net_Pay` varchar(20) DEFAULT NULL,
  `Deductions` varchar(20) DEFAULT NULL,
  `Pay_Mode` varchar(50) DEFAULT NULL,
  `Pay_Period` varchar(50) DEFAULT NULL,
  `PDF_path` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `payslip_db`
--

INSERT INTO `payslip_db` (`id`, `Emp_id`, `Co_Name`, `Payslip_Date`, `Gross_Pay`, `Net_Pay`, `Deductions`, `Pay_Mode`, `Pay_Period`, `PDF_path`) VALUES
(1, '14', '5 Star Cosmetics Limited', '2014-11-18', '1374.51', '1223.87', '150.64', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_14_20141118.pdf'),
(2, '1', '5 Star Cosmetics Limited', '2014-11-18', '1109.33', '687.69', '421.64', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_1_20141118.pdf'),
(3, '5', '5 Star Cosmetics Limited', '2014-11-18', '1109.33', '944.09', '165.24', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_5_20141118.pdf'),
(4, '7', '5 Star Cosmetics Limited', '2014-11-18', '1336.90', '1130.86', '206.04', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_7_20141118.pdf'),
(5, '9', '5 Star Cosmetics Limited', '2014-11-18', '1834.00', '1326.56', '507.44', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_9_20141118.pdf'),
(6, '12', '5 Star Cosmetics Limited', '2014-11-18', '1109.33', '1000.57', '108.76', 'BACS', 'Monthly', 'C:\\xampp\\htdocs\\laravel - Copy\\app\\storage\\payslips\\5 Star Cosmetics Limited_12_20141118.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `resetpassword`
--

CREATE TABLE IF NOT EXISTS `resetpassword` (
  `ref_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resetpassword`
--

INSERT INTO `resetpassword` (`ref_id`, `token`, `created_at`) VALUES
(1, '27Sdx29seYTFI5Y9Aw5TREtaCz1EDqxuZWapZ1DOgPTsJbQUAevW7kw9jMUh', '2014-12-09 03:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `payload`, `last_activity`) VALUES
('91e0d39119b967426f1df67abc7f102006db7418', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiYWx4eDdsWlpoOFAyQUFiMzZVMHQ4aE9BRkVlZXh0dDUxb0JIRlZhVyI7czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQxODk5Mzc5NjtzOjE6ImMiO2k6MTQxODk5MzMxMztzOjE6ImwiO3M6MToiMCI7fX0=', 1418993797),
('d1e4cf1e61daa64b439f1814b4aaab0c272b7b95', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoia2Eya1hkMEI3TkRwazBYRTVUcWZxekdOY05vWEE1RjM1N21BMGhENiI7czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTQxODk5OTI3NjtzOjE6ImMiO2k6MTQxODk5Mzk3MjtzOjE6ImwiO3M6MToiMCI7fX0=', 1418999276),
('dd31a17d483abd619b87423f40c5d7d95c160626', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiNlh4bVZpWjNRbEZtU3Y5ZmczTjhKY0VCMW1HdGszSjdoUEIzYVFoaiI7czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM4OiJsb2dpbl84MmU1ZDJjNTZiZGQwODExMzE4ZjBjZjA3OGI3OGJmYyI7aToxO3M6NToibHRpbWUiO2k6MTQxODk5MzMxMztzOjc6InNlc2NvZGUiO3M6NDA6IjZYeG1WaVozUWxGbVN2OWZnM044SmNFQjFtR3RrM0o3aFBCM2FRaGoiO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDE4OTkzMzEyO3M6MToiYyI7aToxNDE4OTgwMTExO3M6MToibCI7czoxOiIwIjt9fQ==', 1418993313);

-- --------------------------------------------------------

--
-- Table structure for table `ses_db`
--

CREATE TABLE IF NOT EXISTS `ses_db` (
`id` int(10) unsigned NOT NULL,
  `refid` int(11) NOT NULL,
  `lastseen` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `setting_db`
--

CREATE TABLE IF NOT EXISTS `setting_db` (
`id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Value` varchar(5000) NOT NULL,
  `Type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `setting_db`
--

INSERT INTO `setting_db` (`id`, `Name`, `Value`, `Type`) VALUES
(1, 'Default_Pass', 'Welcome@123', NULL),
(2, 'AdminDefault_Pass', 'Admin@123', NULL),
(3, 'LoginDeactAfter', '90', NULL),
(4, 'ResetPassLinkValidity', '7', 'Hours');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `coid` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `contactno` varchar(40) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `passhistory` varchar(255) NOT NULL DEFAULT '',
  `passexp_date` date NOT NULL DEFAULT '2099-01-01',
  `role` varchar(255) NOT NULL,
  `verified_on` date NOT NULL DEFAULT '2001-01-01',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(255) NOT NULL DEFAULT 'system',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `firstlogin` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `coid`, `userid`, `password`, `name`, `address`, `zipcode`, `contactno`, `email`, `dob`, `gender`, `passhistory`, `passexp_date`, `role`, `verified_on`, `status`, `attempts`, `created_by`, `created_at`, `updated_at`, `remember_token`, `last_login`, `firstlogin`) VALUES
(1, '', 'vishal@corientbs.com', '$2y$10$Y6mhsUn.3/geuVYTvjiJO.DJ0mGvLxG3VIs2H.0htp8upVg47hsMq', 'admin', NULL, NULL, NULL, 'vishal@corientbs.com', NULL, NULL, '', '2099-01-01', 'UL1', '2001-01-01', 1, 0, 'system', '2014-11-20 00:34:41', '2014-12-19 08:57:56', 'e9ZpV54D42NamNDP1uJALZpsBKdxBko1a5q8ja6TUYfjGCVzNNS0K1pOCsGk', '2014-12-19 08:57:42', 0),
(2, '', 'staff@example.org', '$2y$10$ehat3c5BYAFWsOVxF/ujt..RomkpCjiy19XhyKpLgeiheuXUA5PhO', 'staff', NULL, NULL, NULL, 'staff@example.org', NULL, NULL, '', '2099-01-01', 'UL2', '2001-01-01', 1, 0, 'system', '2014-11-20 00:34:42', '2014-11-20 00:34:42', NULL, NULL, 1),
(3, '', 'cladmin@example.org', '$2y$10$BVhQHVZoVZAknguaGCnE9.op8cfr9YWlF2d7JU255K2V2DGafUy/e', 'cladmin', NULL, NULL, NULL, 'cladmin@example.org', NULL, NULL, '', '2099-01-01', 'CL1', '2001-01-01', 1, 0, 'system', '2014-11-20 00:34:42', '2014-11-20 00:34:42', NULL, NULL, 1),
(4, '', 'eclient@example.org', '$2y$10$ajBvdEQeFVwDkmx0.UIgjOJqRbReaeXDpWg.BvXS8KOsjVSvgcQTy', 'eclient', NULL, NULL, NULL, 'eclient@example.org', NULL, NULL, '', '2099-01-01', 'CL2', '2001-01-01', 1, 0, 'system', '2014-11-20 00:34:42', '2014-11-20 00:34:42', NULL, NULL, 1),
(38, '1', 'lewis@5starcosmetics.co.uk', '$2y$10$e5FC664SnMLYobteDxcMxugf6Q0hfKbn5GGO/7q5rGwZqPFuH6m6u', 'Lewis', 'Unit Y, Radius Court\nTungsten Business Park\nMaple Drive\nHinckley\nLeicestershire', 'LE10 3BE', '9594789110', 'lewis@5starcosmetics.co.uk', NULL, NULL, '', '2099-01-01', 'CL1', '2001-01-01', 1, 0, 'system', '2014-12-08 01:40:49', '2014-12-09 07:58:17', '3r3jQdXU4uNxezRT7PRNbq62bRpyt115HsLHAZS9LkQHRiSDDgrI6IjpyRSR', '2014-12-09 07:42:14', 0),
(39, '1', '14', '$2y$10$l5mx/RVbAVdzYdOd0uL7L.u.dLZbJkpP56kT2JbxZfLgrqHydpD0i', 'Miss. CE Clarke', 'Xyz,&#10;Abc,&#10;421', '421003', '9029802608', 'xyz@abc.com', NULL, NULL, '', '2099-01-01', 'CL2', '2001-01-01', 1, 0, 'system', '2014-12-08 01:43:41', '2014-12-09 08:00:58', '3GIHNkSwY6UUT2W7xZp2lnESE3OvibC2WIg1UkSH6OkunjVcoGzfreHKvPJK', '2014-12-09 07:59:04', 0),
(40, '2', 'ravi.gowda@travelklinix.com', '$2y$10$NzdhIKvIDXhXeUrt3Xh2EO7Lwr/OH5x0AhGmFEVmJFPS53p1pVhxu', 'Ravi Gowda', 'First Floor\nOasis Health Care\n51 Quinton Park\nCheylesmore\ncoventry', 'CV3 5PZ', '7970966340', 'ravi.gowda@travelklinix.com', NULL, NULL, '', '2099-01-01', 'CL1', '2001-01-01', 1, 0, 'system', '2014-12-08 04:44:45', '2014-12-09 07:38:14', '1T7rPdWhEMrpecNJcltip6Lw7kwjJDKlnooKI9HxAq2kWfa9pTqyO9NlHpk6', '2014-12-09 07:37:20', 0),
(41, '3', 'richard@keyhealthcare.co.uk', '$2y$10$Z0jZmmGfJMf8JizGl5eLhudE.eIkUbpq54Bpb7212y3/5pwo9fBzG', 'Richard', '2nd Floor\n5 Cromwell Park\nWetherby', '421003', '9029929978', 'richard@keyhealthcare.co.uk', NULL, NULL, '', '2099-01-01', 'CL1', '2001-01-01', 1, 0, 'system', '2014-12-08 05:36:45', '2014-12-09 02:02:50', 'fUzwwtprmac6AC8vg6Kbza3JOIJvd0cebfIjTWhL4BgJohye8wpWpjIizV7A', '2014-12-09 02:02:45', 0),
(42, '1', 'tina@corientbs.com', '$2y$10$ssTHyrIIIVSP8EeUy7bUz.lQL5reEe7ZctKf/WIYPIOv9s80HikfG', 'Lewis Bagga', 'Unit Y, Radius Court\nTungsten Business Park\nMaple Drive\nHinckley\nLeicestershire', 'LE10 3BE', '2756659690', 'tina@corientbs.com', NULL, NULL, '', '2099-01-01', 'CL1', '2001-01-01', 1, 0, 'system', '2014-12-19 03:43:30', '2014-12-19 03:51:23', '1uwd70aAtbs4wUvW1PqVwJEYliOTgUedsXQhmqQCFFe0dRrQ2p2ZNA5bstvK', '2014-12-19 03:49:43', 0),
(43, '1', '5', '$2y$10$LDVMTqme80l0uaESmU5SduLPWRxvRYKhVzs8MCX9PxYEjPok8SRjy', 'Lewis', 'D-1712&#10;Mumbai - 83', '400083', '', 'snehal.mule@corientbs.com', NULL, NULL, '', '2099-01-01', 'CL2', '2001-01-01', 1, 0, 'system', '2014-12-19 03:55:11', '2014-12-19 04:48:18', 'ivEdtMILOikAD42TmzgzGB2O8eJR60Jzmo7WKDOAsvXKTw5oH2DcEkbknjYn', '2014-12-19 03:55:41', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_db`
--
ALTER TABLE `access_db`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_db`
--
ALTER TABLE `company_db`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `coname` (`coname`);

--
-- Indexes for table `employee_db`
--
ALTER TABLE `employee_db`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `emp_ref` (`emp_ref`,`coname`);

--
-- Indexes for table `password_reminders`
--
ALTER TABLE `password_reminders`
 ADD KEY `password_reminders_email_index` (`email`), ADD KEY `password_reminders_token_index` (`token`);

--
-- Indexes for table `payslip_db`
--
ALTER TABLE `payslip_db`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `Emp_id` (`Emp_id`,`Co_Name`,`Payslip_Date`);

--
-- Indexes for table `resetpassword`
--
ALTER TABLE `resetpassword`
 ADD UNIQUE KEY `ref_id` (`ref_id`), ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
 ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `ses_db`
--
ALTER TABLE `ses_db`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_db`
--
ALTER TABLE `setting_db`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `Name` (`Name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `coid` (`coid`,`userid`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_db`
--
ALTER TABLE `access_db`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `company_db`
--
ALTER TABLE `company_db`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_db`
--
ALTER TABLE `employee_db`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payslip_db`
--
ALTER TABLE `payslip_db`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ses_db`
--
ALTER TABLE `ses_db`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting_db`
--
ALTER TABLE `setting_db`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
