<?php

class UsersController extends BaseController{

	public function getusers(){
		$users = $this->users();
		return Response::json($users);
	}

	public function getemployees(){
		$users = $this->employees();
		return Response::json($users);
	}
	protected function users(){
		$role = Auth::user()->role;
		switch ($role) {
			case 'UL1':
			$users = User::get();
			break;	
			case 'UL2':
			$users = User::whereIn('role', array('Cl1', 'CL2'))->get();
			break;
			case 'CL1':
			$users = User::where('role', 'CL2')->get();
			break;
			case 'CL2':
			break;
		}
		return $users;
	}


	protected function employees(){
		$role = Auth::user()->role;
		$coid = Auth::user()->coid;
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		switch ($role) {
			case 'CL1':
			$users = DB::table('employee_db')
			->orderBy('name', 'asc')
			->where('coname', '=', $coname)->get();
			break;
			case 'CL2':
			break;
		}
		return $users;
	}

	protected function p60employees(){
		$role = Auth::user()->role;
		$coid = Auth::user()->coid;
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		switch ($role) {
			case 'CL1':
			$users = DB::table('employee_db')
			->join('p60_db', 'employee_db.ninumber', '=', 'p60_db.ninumber')
			->where('employee_db.coname', '=', $coname)
			->distinct()
			->select('employee_db.*')
			->get();
			break;
			case 'CL2':
			break;
		}
		return $users;
	}

protected function ChngPass(){
		$Retval = '';
		$RetCode = '';
		$role = Auth::user()->role;
		$id = Trim(Input::json('id'));
		$user = User::find($id);
		if (Hash::check(Input::json('oldpass'), $user->password)){
			$user->password = Hash::make(Input::json('newpass'));
			if(Input::json('oldpass') != Input::json('newpass')){
			try {
				if($user->firstlogin == 1){
					$user->firstlogin = 0;
				}
				$user->save();
				//$users = $this->users();
				$Retval = 'Updated Successfully...';
				$RetCode = 200;
//return Response::json($users);
			} catch (Exception $e){
				$Retval = 'Error occured while updating';
				$RetCode = 500;
//return Response::json(array('flash' => "Error occured while updating"), 500);
			}

			} else{
		$Retval = 'New Password should not be same as old Password...';
			$RetCode = 500;
	}
		}
		else{
			$Retval = 'Old Password Not Matched, Try Again...';
			$RetCode = 500;
		}

	
		return Response::json(array('flash' => $Retval), $RetCode);
	}
	

	Public function update($id){
		$role = Auth::user()->role;
		$user = User::find($id);
		$user->name = Input::json('name');
		$user->role = Input::json('role');
		try {
			$user->save();
			$users = $this->users();
			return Response::json($users);
		} catch (Exception $e){
			return Response::json(array('flash' => "Error occured while updating"), 500);
		}
		
	}

	protected function ForgotPassword(){
		$rmail = Input::json('remail');
		$ref_id = DB::table('users')->where('email', '=', $rmail)->pluck('id');
		if($ref_id != ''){
		DB::table('ResetPassword')->where('ref_id', '=', $ref_id)->delete();
		$RandomAuth = '';
		while(1 == 1) {
		$RandomAuth = str_random(60);
		$FoundCount =  DB::table('ResetPassword')->where('token', '=', $RandomAuth)->count();
		if($FoundCount == 0){
			break;
		}
		}
		
		DB::table('ResetPassword')->insert(
								array('ref_id' => $ref_id, 
									'token' => $RandomAuth, 
									'created_at' => DB::Raw('NOW()')
									)
								);
		return Response::json(array('flash' => "Password reset details Has been sent to your registered email id."), 200);
		
		}
		else
		{
		return Response::json(array('flash' => "Invalid Email Id"), 500);	
		}
	}

	
}

?>