<?php

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	// public function getRemind()
	// {
	// 	return View::make('password.remind');
	// }

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */

	Public function Verifycaptcha($response, $challenge){
		//require_once(base_path().DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'recaptcha'.DIRECTORY_SEPARATOR.'recaptchalib.php');
		//$publickey = "6LeqFP8SAAAAABzF80ETlbEatHiLeEW_hRvW8Kuc";
		$privatekey = "6LeqFP8SAAAAAPjYuMOp5piftbQgEPHcdNBIFKFX";
		# the response from reCAPTCHA
		$resp = null;
		# the error code from reCAPTCHA, if any
		//$error = null;
		# was there a reCAPTCHA response?
		$resp = file_get_contents("https://www.google.com/recaptcha/api/verify?privatekey=".$privatekey."&challenge=".$challenge."&response=".$response.'&remoteip='.Request::getClientIp());
		$retval = false;
		if(explode ("\n", $resp)[0] == 'true'){
			$retval = true;
		}
		return $retval;
        // $resp = recaptcha_check_answer ($privatekey,
        //                                Request::getClientIp(),
        //                                $challenge,
        //                                $response);

        	// if ($resp->is_valid) {
         //        //echo "You got it!";
         //        return true;
        	// } else {
         //        # set the error code so that we can display it
         //        //$error = $resp->error;
         //        return false;
        	// }
	
	}

	public function postRemind()
	{

		$response = str_replace(' ', '', Input::json('response'));
		$challenge = Input::json('challenge');
		//$responsez = file_get_contents("https://www.google.com/recaptcha/api/verify?privatekey=6LeqFP8SAAAAAPjYuMOp5piftbQgEPHcdNBIFKFX&challenge=".$challenge."&response=".$response.'&remoteip='.Request::getClientIp());		
		if($this->Verifycaptcha($response,$challenge)){

		$Email_Id = Input::only('email');
		DB::table('password_reminders')->where('email', '=', $Email_Id)->delete();
		switch ($response = Password::remind($Email_Id, function($message)
	{
    $message->subject('Password Reminder');
	}))
		{
			case Password::INVALID_USER:
			return Response::json(array('flash' => "Invalid Email Id"), 500);	
				//return Redirect::back()->with('error', Lang::get($response));

			case Password::REMINDER_SENT:
			return Response::json(array('flash' => "Password reset details has been sent to your registered email id."), 200);
				//return Redirect::back()->with('status', Lang::get($response));
		}
		}
		else
		{
			return Response::json(array('flash' => "The characters you entered didn't match the word verification. Please try again."), 500);	
		}

	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		return View::make('password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{

		$response = str_replace(' ', '', Input::json('response'));
		$challenge = Input::json('challenge');
		//$responsez = file_get_contents("https://www.google.com/recaptcha/api/verify?privatekey=6LeqFP8SAAAAAPjYuMOp5piftbQgEPHcdNBIFKFX&challenge=".$challenge."&response=".$response.'&remoteip='.Request::getClientIp());		
		if($this->verifycaptcha($response,$challenge)){
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
			return Response::json(array('flash' => "Token Mismatch try doing password reset again"), 500);	
				//return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
			DB::table('password_reminders')->where('email', '=', $credentials['email'])->delete();
			return Response::json(array('flash' => "Password resetted Successfully..."), 200);
				//return Redirect::to('/');
		}
	}
	else
	{
		return Response::json(array('flash' => "The characters you entered didn't match the word verification. Please try again."), 500);	
	}
	}

}
