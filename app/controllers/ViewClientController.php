<?php
class ViewClientController extends BaseController{
	
	public function viewclient(){
		$ClientInfo='';
		$UserType = '';
		$LoggedUser = Auth::user();
		if ($LoggedUser->role == 'UL1'){
			$UserType = 'CL1';
		}elseif ($LoggedUser->role == 'CL1'){
			$UserType = 'CL2';
		}
		$usertableid = trim(Input::json('id'));
		if ($usertableid != ''){
			$ClientInfo = DB::select("select u.id,u.coid, u.email, u.name as fname, c.coname, u.address, u.zipcode as pcode, u.contactno as cnumber
				from users u inner join company_db c on u.coid=c.id where u.id=?", array($usertableid));
			return Response::json($ClientInfo);
		}else{
			if($LoggedUser->role =='UL1'){
				$ClientInfo = DB::select("select u.id,u.coid,c.coname,u.userid,u.name,u.email,u.role,u.status from users u inner join company_db c on u.coid=c.id where u.Role=?",array($UserType));
			}
			else
			{
				$ClientInfo = DB::select("select u.id,u.coid,c.coname,u.userid,u.name,u.email,u.role,u.status from users u inner join company_db c on u.coid=c.id where u.Role=? and c.id=?",array($UserType,$LoggedUser->coid));
			}
			return Response::json($ClientInfo);
		}
	}


	public function updateclientstatus(){
		$FinalMsg = '';
		$Code = '';

		$usertableid = trim(Input::json('id'));
		if(Auth::user()->id != $usertableid){
		$usertablestatus = trim(Input::json('status'));
	// $currstatus=0;
	// if ($usertablestatus==0){
	// 	$currstatus=1;
	// }
		$op = DB::table('users')
		->where('id', $usertableid)
		->update(array('status' => $usertablestatus));
		$FinalMsg = '';
		if ($usertablestatus==1){
			$FinalMsg = "Client Activated Successfully...";
			$Code = 200;
		}
		else{
			$FinalMsg = "Client Deactivated Successfully...";	
			$Code = 200;
		}
		return Response::json(array('flash' => $FinalMsg), $Code);
	}
	else
	{
		return Response::json(array('flash' => "Can't Deactivate/Activate your own id" ), 500);	
	}
	}

	public function resetpass(){
		$FinalMsg = '';
		$Code = '';

		$usertableid = trim(Input::json('id'));
		if(Auth::user()->id != $usertableid){
		$DefaultPass = DB::table('setting_db')->where('Name', '=', 'Default_Pass')->pluck('Value');
		$op = DB::table('users')
		->where('id', $usertableid)
		->update(array('password' => Hash::make($DefaultPass), 'firstlogin' => 1));
		$FinalMsg = "Password Reseted Successfully... New Password = '".$DefaultPass."'";
		$Code = 200;
		return Response::json(array('flash' => $FinalMsg), $Code);
	}
	else
		{
			return Response::json(array("flash" => "Can't Reset your own password"), 500);	
		}

}
}