<?php
class SignupController extends BaseController{

    // `id` int(10) UNSIGNED AUTO_INCREMENT NOT NULL, 
    // `userid` varchar(255) NOT NULL, 
    // `password` varchar(255) NOT NULL, 
    // `name` varchar(255) NOT NULL, 
    // `address` varchar(1000) NULL, 
    // `zipcode` varchar(50) NULL, 
    // `contactno` varchar(40) NULL, 
    // `email` varchar(255) NULL, 
    // `dob` date NULL, 
    // `gender` varchar(20) NULL, 
    // `passhistory` varchar(255) NOT NULL DEFAULT '', 
    // `passexp_date` date NOT NULL DEFAULT '2099-01-01', 
    // `role` varchar(255) NOT NULL, 
    // `verified_on` date NOT NULL DEFAULT '2001-01-01', 
    // `status` tinyint(1) NOT NULL DEFAULT '1', 
    // `attempts` int NOT NULL DEFAULT '0', 
    // `created_by` varchar(255) NOT NULL DEFAULT 'system', 
    // `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
    // `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
    // `remember_token` varchar(100) NULL, 

	public function signupclient(){

		$Msg = '';
		$Code ='';

		$email = Input::json('email');
		//$password = Input::json('password');
		$name = Input::json('fname');
		$coname = Input::json('coname');
		$address = Input::json('address');
		$pcode = Input::json('pcode');
		$cnumber = Input::json('cnumber');
		if(trim($email)!='' && trim($name)!='' && 
			trim($coname)!='' && trim($address)!='' && 
			trim($pcode)!='' && trim($cnumber)!=''){

			$IsEmailExists = DB::table('users')->where('userid', '=', $email)->
		orwhere('email', '=', $email)->pluck('id');
		$IsCoExists = DB::table('company_db')->where('coname', '=', $coname)->pluck('id');
		if($IsEmailExists=='' && $IsCoExists==''){
			$coid = DB::table('company_db')->insertGetId(
				array(
					'coname' => $coname
					)
				);
			$DefaultClientPass =DB::table('setting_db')->where('Name', '=', 'AdminDefault_Pass')
								->pluck('Value');
			DB::table('users')->insert(
				array(
					'userid' => $email,
					'name' => $name, 
					'coid' => $coid, 
					'address' => $address, 
					'zipcode' => $pcode, 
					'contactno' => $cnumber, 
					'email' => $email, 
					'password' => Hash::make($DefaultClientPass),
					'role' => 'CL1',
					'created_at' => DB::Raw('NOW()')
					// ,
					// 'updated_at' => new DateTime
					)
				);

			
			$Msg='Added Successfully...';
			$Code =200;
		}else{
			//Response::json('User or Company Already There', 400);
			$Msg='User or Company Already There';
			$Code =400;
		}
		
	}else{
		//Response::json('Basic Validation Fail', 400);
		$Msg='Basic Validation Fail';
		$Code =400;
	}
	
	return Response::json(array('flash' => $Msg), $Code);
}


public function updateclient(){

	$Msg = '';
	$Code ='';
	$uid = Input::json('id');
	$coid = Input::json('coid');
	$name = Input::json('fname');
	$coname = Input::json('coname');
	$address = Input::json('address');
	$pcode = Input::json('pcode');
	$cnumber = Input::json('cnumber');
	if(trim($uid)!='' && trim($name)!='' && 
		trim($coid)!='' && 
		trim($coname)!='' && trim($address)!='' && 
		trim($pcode)!='' && trim($cnumber)!=''){

		DB::table('users')
	->where('id', $uid)
	->update(array(
		'name' => $name, 
		'address' => $address, 
		'zipcode' => $pcode, 
		'contactno' => $cnumber, 
		'role' => 'CL1',
		'updated_at' => DB::Raw('NOW()')
		));
	DB::table('company_db')
	->where('id', $coid)
	->update(array('coname' => $coname));
	

	
	$Msg = 'Updated Successfully...';
	$Code = 200;
	
}else{
	//Response::json('Basic Validation Fail...', 400);
	$Msg = 'Basic Validation Fail...';
	$Code =400;
}

return Response::json(array('flash' => $Msg), $Code);
}


public function signupuser(){


	$Msg = '';
	$Code = '';

	$ccode = Input::json('ccode');
	$empref = Input::json('empref');
	$ninumber = Input::json('ninumber');
	$lsalary = Input::json('lsalary');
	$password = Input::json('password');
	$name = Input::json('fname');
	$address = Input::json('address');
	$pcode = Input::json('pcode');
	$email = Input::json('email');
	$cnumber = Input::json('cnumber');
	$IsEmailExists ='';
	$IsEmpCodeExists = '';
	$IsEmpCodeExistsinEmpdb = '';

	if(trim($ccode)!='' && trim($empref)!='' && trim($ninumber)!='' && trim($lsalary)!='' && 
		trim($password)!='' && trim($name)!='' && 
		trim($address)!='' && trim($pcode)!=''){


		
		$IsEmpCodeExists = DB::table('users')->where('userid', '=', $empref)
	->where('coid', '=', $ccode)->pluck('id');
	if (trim($email)!=''){
		$IsEmailExists = DB::table('users')->where('userid', '=', $email)->
		orwhere('email', '=', $email)->pluck('id');
	}
	if($IsEmailExists=='' && $IsEmpCodeExists==''){
		$Getconame = DB::table('company_db')->where('id', '=', $ccode)->pluck('coname');

		$IsEmpCodeExistsinEmpdb = DB::table('employee_db')->where('emp_ref', '=', $empref)
		->where('coname', '=', $Getconame)
		->where('firstpay', '=', $lsalary)
		->where('ninumber', '=', $ninumber)->get();
		if (count($IsEmpCodeExistsinEmpdb)==1){
		//if ($)
		//$Msg = $IsEmpCodeExistsinEmpdb[0]->coname;
	//$GetCoId = DB::table('company_db')->where('coname', '=', $IsEmpCodeExistsinEmpdb[0]->coname)->pluck('id');
			
			DB::table('users')->insert(
				array(
					'userid' => $empref,
					'name' => $name, 
					'coid' => $ccode, 
					'address' => $address, 
					'zipcode' => $pcode, 
					'contactno' => $cnumber, 
					'email' => (TRIM($email)=='' ? NULL : TRIM($email)), 
					'password' => Hash::make($password),
					'role' => 'CL2',
					'created_at' => DB::Raw('NOW()'),
					'updated_at' => DB::Raw('NOW()'),
					'firstlogin' => 0
					)
				);
			
			$Msg='Added Successfully...';
			$Code = 200;
//$Msg= 'Co Entry No Found';
		}
		else
		{
			//$ErrMsg= 'Employee Record Not Found.';
			$Msg='Employee Record Not Found.';
			$Code = 400;
		}
	}
	else
	{
		//$ErrMsg= 'Already in Exists in User database';
		$Msg='Already Exists in User database';
		$Code = 400;

	}
}
else
{
	//$ErrMsg= 'Basic Validation Fail';
	$Msg='Basic Validation Fail';
	$Code = 400;

}
// if($ErrMsg==''){
// 	return Response::json($Msg, 200);
// } else
// {
// 	return Response::json($ErrMsg, 400);
// }
return Response::json(array('flash' => $Msg), $Code);
}
}
?>