<?php

class AuthController extends BaseController{



	public function signup(){
		// if(Auth::attempt(array('email' => Input::json('email') , 'password' => Input::json('password')))){
		// 	// Session::flush();
		// 	$ltime = time();
		// 	$code = csrf_token().str_random(40) . $ltime;
		// 	$usr = Auth::user();
		// 	$access = DB::table('access_db')->select('eroute','name')->where('role', $usr->role)->get();
		// 	$usr->routes = $access;
		// 	$usr->code = $code;
		// 	Session::put('ltime',$ltime);
		// 	Session::put('sescode',$code);
		// 	return Response::json($usr);
		// } else {
		// 	return Response::json(array('flash' => 'Invalid username or password'), 500);
		// }
	}

	public function logout(){
	Session::forget('ltime');
	Session::forget('sescode');
	// $usr = Auth::user();
	// $UpdateVal = User::where('id', $usr->id)->pluck('updated_at');
	//  if ($UpdateVal==''){
	//  	$UpdateVal = DB::raw('null');
	//  }
	Auth::logout();
	//User::where('id', $usr->id)->update(array('updated_at' => $UpdateVal));
	return Response::json(array('flash' => 'Logged Out !'),200);
}

	public function login(){
		if(Auth::attempt(array('userid' => Input::json('email') , 
			'password' => Input::json('password'), 
			'coid' => (trim(Input::json('ccode'))==0 ? '' : trim(Input::json('ccode')))))){
			$usr = Auth::user();
			$LastPayDate = '';
			$LastAccessibleDate = '';
			DB::table('password_reminders')->where('email', '=', $usr->email)->delete();
			if ($usr->status == 0){
				$this->logout();
				return Response::json(array('flash' => 'Account Deactivated...'), 500);
			}
			if ($usr->role == 'CL2'){
			$coname = DB::table('company_db')->where('id', '=', $usr->coid)->pluck('coname');
			$LastPayDate = DB::table('payslip_db')->where('emp_id', '=', $usr->userid)->
			where('co_name', '=', $coname)->pluck('Payslip_Date');
			$LoginDeactAfter = DB::table('setting_db')->where('Name', '=', 'LoginDeactAfter')->pluck('Value');
			$LastAccessibleDate = Date('Y-m-d', strtotime('-'.$LoginDeactAfter.' days'));
			if($LastPayDate < $LastAccessibleDate){
				$this->logout();
				return Response::json(array('flash' => 'Account Deactivated...'), 500);
			}
			}
			// Session::flush();
		$ltime = time();
		//$code = csrf_token().str_random(40) . $ltime;
		$code = csrf_token();
		$access = DB::table('access_db')->select('eroute','name')->where('role', $usr->role)->get();
		$coname = DB::table('company_db')->where('id', $usr->coid)->pluck('coname');
		$usr->coname = $coname;
		$usr->routes = $access;
		$usr->code = $code;
		Session::put('ltime',$ltime);
		Session::put('sescode',$code);
		//Create Directory
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR;
		if(is_dir($dirpath)) {
			File::deleteDirectory($dirpath);
		}
		mkdir($dirpath,'0777', true);
		chmod($dirpath, 0777);
		//Create Directory
		if($usr->firstlogin== 1){
			$usr->firsttime = true;
		}
		else{
			$usr->firsttime = false;
		}
		$updtuser = User::find($usr->id);
		$updtuser->last_login = DB::Raw('NOW()');
		$updtuser->save();
		$updtuser ='';
		return Response::json($usr);
		//return Response::json(array('flash' => $LastPayDate.'_'.$LastAccessibleDate), 500);
	} else {
		return Response::json(array('flash' => 'Invalid username or password'), 500);
	}
}



public function getsession(){
	if (Auth::check()){
	$code = Session::get('sescode');
	// $csrf = $csrf = substr($code, 0,strlen($code)-50);		
	$csrf = $code;		
	if ($csrf == csrf_token()){
		$usr = Auth::user();
		$access = DB::table('access_db')->select('eroute','name')->where('role', $usr->role)->get();
		$coname = DB::table('company_db')->where('id', $usr->coid)->pluck('coname');
		$usr->coname = $coname;
		$usr->routes = $access;
		$usr->code = $code;
		if($usr->firstlogin== 1){
			$usr->firsttime = true;
		}
		else{
			$usr->firsttime = false;
		}
		// if($usr->updated_at=='' && $usr->role=='CL1'){
		// 	$usr->firsttime = true;
		// }
		// else{
		// 	$usr->firsttime = false;
		// }
		return Response::json($usr);	
	} else {
		return Response::json(array('role' => 'guest'), 200);	
	}
	}
	else
	{
		return Response::json(array('role' => 'guest'),200);
	}
	
}
}

?>