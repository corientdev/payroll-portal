<?php
class ViewPaySlipController extends BaseController{


public function viewp32(){
		$retval = '';
		$usr = Auth::user();
		$coid = $usr->coid;
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		$retval = DB::table('p32_db')
			->where('p32_db.co_name', '=', $coname)
			->get();
		return Response::json($retval);
		//return $retval;
	}


public function viewp60(){

		$retval = '';

		$userid = trim(Input::json('userid'));
		$coid = trim(Input::json('coid'));
		// $role = DB::table('users')->where('userid', '=', $userid)->
		// 									where('coid', '=', $coid)->pluck('role');
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		if($userid=='allemployee'){
			$retval = DB::table('p60_db')
			->join('employee_db', 'p60_db.ninumber', '=', 'employee_db.ninumber')
			->where('employee_db.coname', '=', $coname)
			->get(array('p60_db.id', 'employee_db.name', 'employee_db.ninumber', 'p60_db.year', 'p60_db.pay', 'p60_db.taxdeducted'));
		}
		else{
			$retval = DB::table('p60_db')
			->join('employee_db', 'p60_db.ninumber', '=', 'employee_db.ninumber')
			->where('employee_db.emp_ref', '=', $userid)
			->where('employee_db.coname', '=', $coname)
			->get(array('p60_db.id', 'employee_db.name', 'employee_db.ninumber', 'p60_db.year', 'p60_db.pay', 'p60_db.taxdeducted'));
		}
		// if(trim($role)=='CL2'){
		
		// }elseif (trim($role)=='CL1') {
		// 	$retval = DB::table('payslip_db')->where('co_name', '=', $coname)->
		// 									get(array('id', 'Payslip_date', 
		// 										'Gross_Pay', 'Net_Pay', 'Deductions', 
		// 										'Pay_Mode', 'Pay_Period'));
		// }elseif (trim($role)=='CL2') {
		// 	# code...
		// }
		return Response::json($retval);
		//return $retval;
	}

	

	protected function getpayruns(){
		$userid = trim(Input::json('userid'));
		$usr = Auth::user();
		$coname = DB::table('company_db')->where('id', '=', $usr->coid)->pluck('coname');
		switch ($usr->role) {
			case 'CL1':
			$querry = DB::table('payslip_db')
			->where('co_name', '=', $coname)
			->orderBy('payslip_date', 'desc')
			->distinct();
			
			if($userid != 'allemployee'){
				$querry->where('emp_id', '=', $userid);
			}

			$RetVal = $querry->get(array('Payslip_Date'));
			break;
			case 'CL2':
			break;
		}
		return $RetVal;
	}


	public function viewpayslip(){

		$retval = '';

		$userid = trim(Input::json('userid'));
		$coid = trim(Input::json('coid'));
		$payrun = trim(Input::json('payrun'));
		// $role = DB::table('users')->where('userid', '=', $userid)->
		// 									where('coid', '=', $coid)->pluck('role');
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		$querry = null;
		$querry = DB::table('payslip_db')
			->join('employee_db', 'payslip_db.Emp_id', '=', 'employee_db.emp_ref')
			->where('payslip_db.co_name', '=', $coname)
			->orderBy('payslip_date', 'desc');
			if($payrun != 'all'){
				$querry->where('Payslip_Date', '=', $payrun);
			}

			if($userid != 'allemployee'){
				$querry->where('emp_id', '=', $userid);
			}
			$retval = $querry->get(array('payslip_db.id', 'employee_db.name', 'Payslip_date', 'Gross_Pay', 'Net_Pay', 'Deductions', 'Pay_Mode', 'Pay_Period'));
		// if($userid=='allemployee'){
		// 	$retval = DB::table('payslip_db')
		// 	->join('employee_db', 'payslip_db.Emp_id', '=', 'employee_db.emp_ref')
		// 	->where('payslip_db.co_name', '=', $coname)
		// 	->get(array('payslip_db.id', 'employee_db.name', 'Payslip_date', 'Gross_Pay', 'Net_Pay', 'Deductions', 'Pay_Mode', 'Pay_Period'));
		// }
		// else{
		// 	$retval = DB::table('payslip_db')
		// 	->join('employee_db', 'payslip_db.Emp_id', '=', 'employee_db.emp_ref')
		// 	->where('emp_id', '=', $userid)
		// 	->where('payslip_db.co_name', '=', $coname)
		// 	->get(array('payslip_db.id', 'employee_db.name', 'Payslip_date', 'Gross_Pay', 'Net_Pay', 'Deductions', 'Pay_Mode', 'Pay_Period'));
		// }
		// if(trim($role)=='CL2'){
		
		// }elseif (trim($role)=='CL1') {
		// 	$retval = DB::table('payslip_db')->where('co_name', '=', $coname)->
		// 									get(array('id', 'Payslip_date', 
		// 										'Gross_Pay', 'Net_Pay', 'Deductions', 
		// 										'Pay_Mode', 'Pay_Period'));
		// }elseif (trim($role)=='CL2') {
		// 	# code...
		// }
		return Response::json($retval);
		//return $retval;
	}

	public function dwnldpayslip(){
		$usr = Auth::user();
		$retval = '';
		$pdfpath='';

		$payslipid = trim(Input::json('id'));
		$userid = trim(Input::json('userid'));
		$coid = trim(Input::json('coid'));
		
		
		
		//$PDF_path = DB::table('payslip_db')->where('id', '=', $payslipid)->pluck('PDF_path');

		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		// $role = DB::table('users')->where('userid', '=', $userid)->
		// 									where('coid', '=', $coid)->pluck('role');
		// if(trim($role)=='CL2'){
		if($userid == 'allemployee'){
			$pdfpath = DB::table('payslip_db')->where('co_name', '=', $coname)->
			where('id', '=', $payslipid)->pluck('PDF_path');
		}
		else
		{
			$pdfpath = DB::table('payslip_db')->where('emp_id', '=', $userid)->
			where('co_name', '=', $coname)->
			where('id', '=', $payslipid)->pluck('PDF_path');
		}
		// }elseif (trim($role)=='CL1') {
		// 	$pdfpath = DB::table('payslip_db')->where('emp_id', '=', $userid)->
		// 										where('co_name', '=', $coname)->
		// 									where('id', '=', $payslipid)->get(array('PDF_path'));
		// }elseif (trim($role)=='CL2') {
		// 	# code...
		// }
		
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR;
		//$retval = str_random(12).'.pdf';
		$retval = basename($pdfpath);
		if(!copy($pdfpath,$dirpath.$retval)){
			$retval='';
		}

		return '/userdata/'.$usr['id'].'/'.$retval;
		//return $retval;
	}

	public function dwnldp60(){
		$usr = Auth::user();
		$retval = '';
		$pdfpath='';

		$payslipid = trim(Input::json('id'));
		$userid = trim(Input::json('userid'));
		$coid = trim(Input::json('coid'));
		
		
		
		//$PDF_path = DB::table('payslip_db')->where('id', '=', $payslipid)->pluck('PDF_path');

		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		// $role = DB::table('users')->where('userid', '=', $userid)->
		// 									where('coid', '=', $coid)->pluck('role');
		// if(trim($role)=='CL2'){
		if($userid == 'allemployee'){
			$pdfpath = DB::table('p60_db')->where('co_name', '=', $coname)->
			where('id', '=', $payslipid)->pluck('pdf_path');
		}
		else
		{
			$pdfpath = DB::table('p60_db')->where('employee_db.emp_ref', '=', $userid)
			->join('employee_db', 'p60_db.ninumber', '=', 'employee_db.ninumber')
			->where('p60_db.co_name', '=', $coname)
			->where('p60_db.id', '=', $payslipid)->pluck('pdf_path');
		}
		// }elseif (trim($role)=='CL1') {
		// 	$pdfpath = DB::table('payslip_db')->where('emp_id', '=', $userid)->
		// 										where('co_name', '=', $coname)->
		// 									where('id', '=', $payslipid)->get(array('PDF_path'));
		// }elseif (trim($role)=='CL2') {
		// 	# code...
		// }
		
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR;
		//$retval = str_random(12).'.pdf';
		$retval = basename($pdfpath);
		if(!copy($pdfpath,$dirpath.$retval)){
			$retval='';
		}

		return '/userdata/'.$usr['id'].'/'.$retval;
		//return $retval;
	}

	public function dwnldp32(){
		$retval = '';
		$pdfpath='';
		$usr = Auth::user();
		$payslipid = trim(Input::json('id'));
		$coid = $usr->coid;
		
		$coname = DB::table('company_db')->where('id', '=', $coid)->pluck('coname');
		
		
			$pdfpath = DB::table('p32_db')
			->where('co_name', '=', $coname)
			->where('id', '=', $payslipid)
			->pluck('pdf_path');
		
				
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR;
		//$retval = str_random(12).'.pdf';
		$retval = basename($pdfpath);
		if(!copy($pdfpath,$dirpath.$retval)){
			$retval='';
		}

		return '/userdata/'.$usr['id'].'/'.$retval;
		//return $retval;
	}

}
?>