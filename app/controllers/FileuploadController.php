<?php

class FileuploadController extends BaseController{
// file,Final,uniquekey
public function getcompany(){
		return DB::table('company_db')->distinct()->get(array('coname'));
	}
	public function getyears(){
		return range(date('Y'), date('Y') -10);
	}
	public function uploadp60(){
		$RetVal = '';
		$Code = '';

		require_once(base_path().DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'fpdf'.DIRECTORY_SEPARATOR.'fpdf.php');
		require_once(base_path().DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'fpdi'.DIRECTORY_SEPARATOR.'fpdi.php');

		$allposteddata = Input::all();
		$uniquekey = $allposteddata['uniquekey'];
		$final = $allposteddata['Final'];
		$Co_name = $allposteddata['coname'];
		$year = $allposteddata['year'];
		$usr = Auth::user();
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'folder_'.
		str_replace('.','',$uniquekey).DIRECTORY_SEPARATOR;
		$filepath='file_'.str_replace('.','',$uniquekey);

		$upload_success = true;
		try {
			if(strtolower(Input::file('file')->getClientOriginalExtension()) == 'xml' || strtolower(Input::file('file')->getClientOriginalExtension()) == 'pdf')
			{
			Input::file('file')->move($dirpath, $filepath.'.'
				.Input::file('file')->getClientOriginalExtension());
			} else{
				$upload_success = false;
				$RetVal = 'Only XML and PDF Allowed';
				$Code =  400;	
			}
		} catch(Exception $e) {
			$upload_success = false;
		}	

		$Emp_id='';
		//$Co_name='';
		//$year='';
		$Payslip_Date='';
		$Ni_Number='';
		$Emp_Name='';
		$FirstPay='';
		$GrossPay='';
		$PaymentMethod='';
		$PaymentPeriod='';
//Delete Later
		$PDFName='';
		$rowcount='';

		if ($final=='true') {
			$temp = file_get_contents($dirpath.$filepath.'.xml');
			$readxml  = simplexml_load_string($temp); 
//PAGES COUNT
//$vis=$readxml->PAGES->count();
//PAGE COUNT 1
//$vis=$readxml->PAGE->count();
//SECTIONS 1
//$vis=$readxml->PAGE[0]->SECTIONS->count();
//SECTION 1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION->count();
//COLUMNS 1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS->count();
//COLUMN >1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN->count();
//NAME >1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
//DETAIL
			$pdf = new FPDI();
			$pagecount = $pdf->setSourceFile($dirpath.$filepath.'.pdf');
			if ($pagecount==$readxml->PAGE->count()){
				for ($i=0;$i<$readxml->PAGE->count();$i++)
				{
// $Emp_id='';
// $Co_name='';
// $Payslip_Date='';
// $PDFName='';
					for($j=0;$j<$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN->count();$j++){

						switch ($readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j]['NAME']) {

							case "P14.NINumber1":
							$NINumber1=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "P14.NINumber2":
							$NINumber2=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "P14.NINumber3":
							$NINumber3=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "P14.NINumber4":
							$NINumber4=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "P14.NINumber5":
							$NINumber5=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.TotalTaxTD":
							$TotalTaxTD=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.TaxGrossTD":
							$TaxGrossTD=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.Initials":
							$Initials=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;
							case "P14.Surname":
							$Surname=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[1]->COLUMNS[0]->COLUMN[$j];
							break;

						}








// if(==''){
// 	}
//HEAD

//$Payslip_Date=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
//$Co_name=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
					}
					$ninumber = $NINumber1.$NINumber2.$NINumber3.$NINumber4.$NINumber5;
					$rowcount = DB::table('p60_db')->where('year', '=', $year)->
					where('co_name', '=', $Co_name)->
					where('ninumber', '=', $ninumber)->pluck('id');

					$Iscofound = DB::table('company_db')->where('coname', '=', $Co_name)->pluck('id');
					if($Iscofound>0 && trim($Iscofound)!=''){
						if($rowcount==''){
							$new_pdf = new FPDI();
							$new_pdf->AddPage();
							$new_pdf->setSourceFile($dirpath.$filepath.'.pdf');
							$new_pdf->useTemplate($new_pdf->importPage($i+1));
							try {
//$PDFName = storage_path().'\\payslips\\'.preg_replace("/[^a-zA-Z0-9 ]+/", "", $Co_name).'_'preg_replace("/[^a-zA-Z0-9 ]+/", "", $Emp_id).'_'.preg_replace("/[^a-zA-Z0-9 ]+/", "", $Payslip_Date).'.pdf';
								$PCo_Name=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Co_name);
								$PEmp_id=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Emp_id);
								$PPayslip_Date=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Payslip_Date);
								$PDFPath = storage_path().DIRECTORY_SEPARATOR.'p60'.DIRECTORY_SEPARATOR;
								if(!is_dir($PDFPath)) {
									mkdir($PDFPath);
								}
								$PDFName = $PDFPath.$PCo_Name.'_'.$ninumber.'_'.$year.'.pdf';
								$new_pdf->Output($PDFName, "F");
//$new_pdf="";
//echo "Page ".$i." split into ".$PDFName."<br />\n";
							} catch (Exception $e) {
//echo 'Caught exception: ', $e->getMessage(), "\n";
							}
// $Emp_id='';
// $Co_name='';
// $Payslip_Date='';
// $PDFName='';
							// $frstuser = DB::table('employee_db')->
							// where('emp_ref', '=', $Emp_id)->
							// where('coname', '=', $Co_name)->pluck('id');
							// if($frstuser==''){
							// 	DB::table('employee_db')->insert(
							// 		array('emp_ref' => $Emp_id, 'coname' => $Co_name, 'name' => $Emp_Name, 
							// 			'ninumber' => $Ni_Number, 'firstpay' => $FirstPay)
							// 		);
							// }
							// else
							// {
							// 	DB::table('employee_db')->where('id', $frstuser)->update(array('firstpay' => $FirstPay));
							// }
							DB::table('p60_db')->insert(
								array('Co_Name' => $Co_name, 
									'name' => $Initials.' '.$Surname, 
									'ninumber' => $ninumber, 
									'year' => $year, 
									'pay' => $TaxGrossTD, 
									'taxdeducted' => $TotalTaxTD, 
									'pdf_path' => $PDFName)
								);

						}
						else
						{
//pdf already uploaded
							$RetVal = 'Same form already uploaded...';
							$Code =  400;
							$upload_success = false;
						}

					}
					else
					{
//Co Not Exista
						//return Response::json('Company Not Exists', 400);
						$RetVal = $Co_name.'Company Not Exists';
						$Code =  400;
						$upload_success = false;
					}

				}
			}
			else
			{
//PDF and XML Count not matched.
				//return Response::json('PDF and XML Count not matched', 400);
				$RetVal = 'PDF and XML Count not matched';
				$Code =  400;
				$upload_success = false;

			}

		}
		if($upload_success) {

			//return Response::json('Successfully uploaded...', 200);
			$RetVal = 'Successfully uploaded...';
			$Code =  200;
		} else {
			//return Response::json('Error try uploading again', 400);
			if ($Code == ''){
			$RetVal = 'Error try uploading again';
			$Code =  400;
			}
		}
		if($final == 'true' || $Code != 200){
			return Response::json(array('flash' => $RetVal), $Code);
			} else {
			return Response::json('false',201);
			}
		
	}

public function uploadp32(){

		$RetVal = '';
		$Code = 201;
		$allposteddata = Input::all();
		$uniquekey = $allposteddata['uniquekey'];
		$final = $allposteddata['Final'];
		$usr = Auth::user();
				$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'folder_'.
		str_replace('.','',$uniquekey).DIRECTORY_SEPARATOR;
		$filepath='file_'.str_replace('.','',$uniquekey);

		$upload_success = true;
		try {
			if(strtolower(Input::file('file')->getClientOriginalExtension()) == 'xml' || strtolower(Input::file('file')->getClientOriginalExtension()) == 'pdf')
			{
			Input::file('file')->move($dirpath, $filepath.'.'
				.Input::file('file')->getClientOriginalExtension());
			} else{
				$upload_success = false;
				$RetVal = 'Only XML and PDF Allowed';
				$Code =  400;	
			}
		} catch(Exception $e) {
			$upload_success = false;
		}	

		$Emp_id='';
		$Co_name='';
		$Payslip_Date='';
		$Ni_Number='';
		$Emp_Name='';
		$FirstPay='';
		$GrossPay='';
		$PaymentMethod='';
		$PaymentPeriod='';
//Delete Later
		//$PDFName='';
		//$rowcount='';

		if ($final=='true') {
			$PDFPath = storage_path().DIRECTORY_SEPARATOR.'p32'.DIRECTORY_SEPARATOR;
			$temp = file_get_contents($dirpath.$filepath.'.xml');
			$readxml  = simplexml_load_string($temp); 
			//$readxml);
			// var_dump($readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[0]['NAME']);
			$coname = $readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[3];
			

			$DateTo = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[0];
			$TotalTax = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[1];
			$TAX_MONTH_FROM = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[2];
			$TAX_MONTH_TO = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[3];
			$DateFrom = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[4];
			$StudentLoanPaid = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[5];
			$NetTax = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[6];
			$TotalNIC = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[7];
			$AmtDue = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[8];
			$NIToPay = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[9];
			$EmploymentAllowance = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[10];
			$SMPRecovered = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[11];
			$SPPRecovered = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[12];
			$ASPPRecovered = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[13];
			$ASPPCompensation = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[14];
			$SAPCompensation = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[15];
			$NetNI = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[16];
			$TotalNICDeductions = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[17];
			$SAPRecovered = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[18];
			$SPPCompensation = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[19];
			$SMPCompensation = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[20];
			$TaxRefundReceived = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[21];
			$SxPFundingReceived = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[22];
			$SMPPaid = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[23];
			$SPPPaid = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[24];
			$ASPPPaid = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[25];
			$StatutoryCompensation = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[26];
			$SAPPaid = $readxml->PAGE[0]->SECTIONS[0]->SECTION[2]->COLUMNS[0]->COLUMN[27];

			File::move($dirpath.$filepath.'.pdf', $PDFPath.preg_replace("/[^a-zA-Z0-9 ]+/", "", $coname).'_'.$DateFrom.'_'.$DateTo.'.pdf');
			$ifcompanyexists = DB::table('company_db')->
							where('coname', '=', $coname)->pluck('id');
			if($ifcompanyexists > 0){
				$rowcount = DB::table('p32_db')->where('co_name', '=', $coname)->
					where('datefrom', '=', $DateFrom)->
					where('dateto', '=', $DateTo)->pluck('id');
				if($rowcount < 1){
				DB::table('p32_db')->insert(
								array(
									'co_name' => $coname,
									'datefrom' => $DateFrom,
									'dateto' => $DateTo,
									'tax_month_from' => $TAX_MONTH_FROM,
									'tax_month_to' => $TAX_MONTH_TO,
									'totaltax' => $TotalTax,
									'studentloanpaid' => $StudentLoanPaid,
									'nettax' => $NetTax,
									'totalnic' => $TotalNIC,
									'employmentallowance' => $EmploymentAllowance,
									'smprecovered' => $SMPRecovered,
									'smpcompensation' => $SMPCompensation,
									'spprecovered' => $SPPRecovered,
									'sppcompensation' => $SPPCompensation,
									'aspprecovered' => $ASPPRecovered,
									'asppcompensation' => $ASPPCompensation,
									'saprecovered' => $SAPRecovered,
									'sapcompensation' => $SAPCompensation,
									'totalnicdeductions' => $TotalNICDeductions,
									'netni' => $NetNI,
									'nitopay' => $NIToPay,
									'amtdue' => $AmtDue,
									'taxrefundreceived' => $TaxRefundReceived,
									'sxpfundingreceived' => $SxPFundingReceived,
									'smppaid' => $SMPPaid,
									'spppaid' => $SPPPaid,
									'aspppaid' => $ASPPPaid,
									'sappaid' => $SAPPaid,
									'statutorycompensation' => $StatutoryCompensation,
									'pdf_path' => $PDFPath.preg_replace("/[^a-zA-Z0-9 ]+/", "", $coname).'_'.$DateFrom.'_'.$DateTo.'.pdf'

									)
								);
			$RetVal = 'Successfully uploaded...';
			$Code =  200;
		} else {
			$RetVal = 'Same form already uploaded...';
			$Code =  400;
		}
			}else
			{
				$RetVal = 'Company Not exists';
				$Code =  400;
			}

		}

		if($final == 'true' || $Code != 200){
			return Response::json(array('flash' => $RetVal), $Code);
			} else {
			return Response::json('false',201);
			}

}


	public function uploadpslip(){

		$RetVal = '';
		$Code = '';

		require_once(base_path().DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'fpdf'.DIRECTORY_SEPARATOR.'fpdf.php');
		require_once(base_path().DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'fpdi'.DIRECTORY_SEPARATOR.'fpdi.php');

		$allposteddata = Input::all();
		$uniquekey = $allposteddata['uniquekey'];
		$final = $allposteddata['Final'];
		$usr = Auth::user();
		$dirpath = public_path().DIRECTORY_SEPARATOR.'userdata'.DIRECTORY_SEPARATOR.$usr['id'].DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'folder_'.
		str_replace('.','',$uniquekey).DIRECTORY_SEPARATOR;
		$filepath='file_'.str_replace('.','',$uniquekey);

		$upload_success = true;
		try {
			if(strtolower(Input::file('file')->getClientOriginalExtension()) == 'xml' || strtolower(Input::file('file')->getClientOriginalExtension()) == 'pdf')
			{
				Input::file('file')->move($dirpath, $filepath.'.'
					.Input::file('file')->getClientOriginalExtension());	
			} else{
				$upload_success = false;
				$RetVal = 'Only XML and PDF Allowed';
				$Code =  400;	
			}
			
		} catch(Exception $e) {
			$upload_success = false;
		}	

		$Emp_id='';
		$Co_name='';
		$Payslip_Date='';
		$Ni_Number='';
		$Emp_Name='';
		$FirstPay='';
		$GrossPay='';
		$PaymentMethod='';
		$PaymentPeriod='';
//Delete Later
		$PDFName='';
		$rowcount='';

		if ($final=='true') {
			$temp = file_get_contents($dirpath.$filepath.'.xml');
			$readxml  = simplexml_load_string($temp); 
//PAGES COUNT
//$vis=$readxml->PAGES->count();
//PAGE COUNT 1
//$vis=$readxml->PAGE->count();
//SECTIONS 1
//$vis=$readxml->PAGE[0]->SECTIONS->count();
//SECTION 1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION->count();
//COLUMNS 1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS->count();
//COLUMN >1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN->count();
//NAME >1
//$vis=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
//DETAIL
			$pdf = new FPDI();
			$pagecount = $pdf->setSourceFile($dirpath.$filepath.'.pdf');
			if ($pagecount==$readxml->PAGE->count()){
				FOR ($i=0;$i<$readxml->PAGE->count();$i++)
				{
// $Emp_id='';
// $Co_name='';
// $Payslip_Date='';
// $PDFName='';
					for($j=0;$j<$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN->count();$j++){

						switch ($readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j]['NAME']) {

							case "Employees.Reference":
							$Emp_id=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "CompanyDetails.Name":
							$Co_name=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "DateParameters.ProcessDate":
							$Payslip_Date=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.NINumber":
							$Ni_Number=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.TitleAndName":
							$Emp_Name=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "CurrentPay.RndNetPay":
							$FirstPay=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "CurrentPay.TotalGross":
							$GrossPay=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.PaymentMethod":
							$PaymentMethod=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;
							case "Employees.PaymentPeriod":
							$PaymentPeriod=$readxml->PAGE[$i]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[$j];
							break;


						}








// if(==''){
// 	}
//HEAD

//$Payslip_Date=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
//$Co_name=$readxml->PAGE[0]->SECTIONS[0]->SECTION[0]->COLUMNS[0]->COLUMN[1]['NAME'];
					}
					$rowcount = DB::table('payslip_db')->where('Emp_id', '=', $Emp_id)->
					where('Co_Name', '=', $Co_name)->
					where('Payslip_Date', '=', $Payslip_Date)->pluck('id');

					$Iscofound = DB::table('company_db')->where('coname', '=', $Co_name)->pluck('id');
					if($Iscofound>0 && trim($Iscofound)!=''){
						if($rowcount==''){
							$new_pdf = new FPDI();
							$new_pdf->AddPage();
							$new_pdf->setSourceFile($dirpath.$filepath.'.pdf');
							$new_pdf->useTemplate($new_pdf->importPage($i+1));
							try {
//$PDFName = storage_path().'\\payslips\\'.preg_replace("/[^a-zA-Z0-9 ]+/", "", $Co_name).'_'preg_replace("/[^a-zA-Z0-9 ]+/", "", $Emp_id).'_'.preg_replace("/[^a-zA-Z0-9 ]+/", "", $Payslip_Date).'.pdf';
								$PCo_Name=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Co_name);
								$PEmp_id=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Emp_id);
								$PPayslip_Date=preg_replace("/[^a-zA-Z0-9 ]+/", "", $Payslip_Date);
								$PDFPath = storage_path().DIRECTORY_SEPARATOR.'payslips'.DIRECTORY_SEPARATOR;
								if(!is_dir($PDFPath)) {
									mkdir($PDFPath);
								}
								$PDFName = $PDFPath.$PCo_Name.'_'.$PEmp_id.'_'.$PPayslip_Date.'.pdf';
								$new_pdf->Output($PDFName, "F");
//$new_pdf="";
//echo "Page ".$i." split into ".$PDFName."<br />\n";
							} catch (Exception $e) {
//echo 'Caught exception: ', $e->getMessage(), "\n";
							}
// $Emp_id='';
// $Co_name='';
// $Payslip_Date='';
// $PDFName='';
							$frstuser = DB::table('employee_db')->
							where('emp_ref', '=', $Emp_id)->
							where('coname', '=', $Co_name)->pluck('id');
							if($frstuser==''){
								DB::table('employee_db')->insert(
									array('emp_ref' => $Emp_id, 'coname' => $Co_name, 'name' => $Emp_Name, 
										'ninumber' => $Ni_Number, 'firstpay' => $FirstPay)
									);
							}
							else
							{
								DB::table('employee_db')->where('id', $frstuser)->update(array('firstpay' => $FirstPay));
							}
							DB::table('payslip_db')->insert(
								array('Emp_id' => $Emp_id, 
									'Co_Name' => $Co_name, 
									'Payslip_Date' => $Payslip_Date, 
									'Gross_Pay' => $GrossPay, 
									'Net_Pay' => $FirstPay, 
									'Deductions' => bcsub($GrossPay,$FirstPay,2), 
									'Pay_Mode' => $PaymentMethod, 
									'Pay_Period' => $PaymentPeriod, 
									'PDF_path' => $PDFName)
								);

						}
						else
						{
//pdf already uploaded
							$RetVal = 'pdf already uploaded';
							$Code =  400;
							$upload_success = false;
						}

					}
					else
					{
//Co Not Exista
						//return Response::json('Company Not Exists', 400);
						$RetVal = 'Company Not Exists';
						$Code =  400;
						$upload_success = false;
					}

				}
			}
			else
			{
//PDF and XML Count not matched.
				//return Response::json('PDF and XML Count not matched', 400);
				$RetVal = 'PDF and XML Count not matched';
				$Code =  400;
				$upload_success = false;

			}

		}
		if($upload_success) {

			//return Response::json('Successfully uploaded...', 200);
			$RetVal = 'Successfully uploaded...';
			$Code =  200;
		} else {
			//return Response::json('Error try uploading again', 400);
			if ($Code == ''){
			$RetVal = 'Error try uploading again';
			$Code =  400;
			}
		}
		if($final == 'true' || $Code != 200){
			return Response::json(array('flash' => $RetVal), $Code);
			} else {
			return Response::json('false',201);
			}
		

	}

}

?>