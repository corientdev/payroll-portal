<!doctype html>
<base href="/">
<html lang="en" ng-app="app">
<head>
  <meta charset="UTF-8">
  <title>Welcome to Corient Payroll Self Service Portal</title>
  <!-- <link rel="stylesheet" href="packages/components/foundation/css/foundation.min.css"> -->
  <!-- <script type="text/javascript" src="packages/bower/headjs/dist/1.0.0/head.min.js" data-headjs-load="js/boot.js"></script> -->
  <style type="text/css">
  input[type=text], input[type=password], input[type=email], input[type=tel] {
    height: 32px !important;
}
</style>
<script type="text/javascript" src="packages/bower/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="packages/bower/bootstrap/dist/js/bootstrap.js"></script>
<script src="packages/bower/toaster/angular.min.js" ></script>
  <script src="packages/bower/toaster/angular-animate.min.js" ></script>
  <script src="packages/bower/toaster/toaster.js"></script>
  <link rel="stylesheet" href="packages/bower/toaster/toaster.css" />

<link rel="stylesheet" type="text/css" href="packages/bower/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="packages/bower/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css">
  <!-- <link rel="stylesheet" href="packages/bower/normalize.css/normalize.css"> -->
  
  
  <script type="text/javascript" src="packages/bower/angularjs/angular.min.js"></script>
  <script type="text/javascript" src="packages/bower/angular-sanitize/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="packages/bower/angular-bootstrap/ui-bootstrap.js"></script>
  <script type="text/javascript" src="packages/bower/angular-fileupload/angular-file-upload.min.js"></script>
  <script type="text/javascript" src="//www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
  <script type="text/javascript" src="packages/bower/recaptcha/angular-recaptcha.js"></script>
  <!-- <script type="text/javascript" src="packages/bower/angular-route/angular-route.js"></script> -->
  <script type="text/javascript" src="packages/bower/angular-ui-router/release/angular-ui-router.min.js"></script>
  <script type="text/javascript" src="packages/bower/ngStorage/ngStorage.min.js"></script>
  <script type="text/javascript" src="packages/bower/angular-permission/angular-permission.js"></script>
  <script type="text/javascript" src="packages/bower/underscore/underscore-min.js"></script>
  <script type="text/javascript" src="packages/bower/angular-smart-table/dist/smart-table.min.js"></script>
  <script type="text/javascript" src="packages/bower/angular-validator/angular-validator.js"></script>

  <link rel="stylesheet" type="text/css" href="css/app.css">
  <script type="text/javascript" src="js/app.js"></script>
  <script type="text/javascript" src="js/appConfig.js"></script>
  <script type="text/javascript" src="js/controllers/FileuploadController.js"></script>
  <script type="text/javascript" src="js/controllers/ForgotPassController.js"></script>
  <script type="text/javascript" src="js/controllers/ResetPassController.js"></script>
  <script type="text/javascript" src="js/controllers/EditEndUserController.js"></script>
  <script type="text/javascript" src="js/controllers/ViewPayslipController.js"></script>
  <script type="text/javascript" src="js/controllers/viewclientController.js"></script>
  <script type="text/javascript" src="js/controllers/HomeController.js"></script>
  <script type="text/javascript" src="js/controllers/LoginController.js"></script>
  <script type="text/javascript" src="js/controllers/SignupController.js"></script>
  <script type="text/javascript" src="js/controllers/baseController.js"></script>
  <script type="text/javascript" src="js/controllers/UserLogoutController.js"></script>
  <script type="text/javascript" src="js/controllers/ContactController.js"></script>
  <script type="text/javascript" src="js/controllers/ConfirmWhoController.js"></script>
  <script type="text/javascript" src="js/directives/showsMessageWhenHovered.js"></script>
  <script type="text/javascript" src="js/directives/ManageModal.js"></script>
  <script type="text/javascript" src="js/services/AuthenticationService.js"></script>
  <script type="text/javascript" src="js/services/BookService.js"></script>
  <script type="text/javascript" src="js/services/FlashService.js"></script>
  <script type="text/javascript" src="js/services/SessionService.js"></script>

<!-- Adapt Strap -->
<script src="packages/bower/adapt-strap/adapt-strap.min.js"></script>
<script src="packages/bower/adapt-strap/adapt-strap.tpl.min.js"></script>
<link rel="stylesheet" href="packages/bower/adapt-strap/adapt-strap.min.css"/>
<!-- Adapt Strap -->

<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>

  <script>
  angular.module("app").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
  </script>
</head>
<body ng-controller="baseController">
<nav class="navbar navbar-fixed-top" role="navigation" style="background-color:white; border:none;" ng-cloak>
  <div class="navbar-inner">
    <div class="navbar-header">
     <!-- <div class="navbar-brand" href="#">
        <div class="navbar-brand" style="background:url('img/sh_logo_med.png') no-repeat; height:75px; width:275px;margin-left:20px;" href="#/home">

        </div>
      </div> -->
      <div class="navbar-brand" style="background:url('img/sh_logo_med.png') no-repeat; height:75px; width:275px;margin-left:20px;" href="#/home">

        </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  >
    <br/>
      <ul class="nav navbar-right" >
        <li><a class="" href="/home">Home</a></li>
        <li><a class="" href="/contactus">Contact Us</a></li>
        <!-- <li><a class="" href="#/contact"> Contact</a></li>
        <li><a class="" href="#/about"> About</a></li> -->
        <!-- <li class="dropdown">
          <a class="" data-toggle="dropdown" href="" ng-show = "user.name.length>0">Actions<b class="caret"></b></a>
          <ul class="dropdown-menu"> -->
            <li ng-repeat="menu in user.routes">
                <a href="{{ menu.eroute }}">{{ menu.name }}</a>
            </li>
          <!-- </ul>
        </li> -->
        <li><a class="" href="/login" ng-hide = "user.name.length>0"> Log In</a></li>
        <li class="dropdown" ng-show = "user.name.length>0">
          <a class="" href=""  data-toggle="dropdown"> {{ user.name }} <b class="caret"></b></a>
          <!-- <small>({{user.role}})</small> -->
          <ul class="dropdown-menu dropdown-menu-left">
            <li><a href="/uchngpass"><small>Change Password</small></a></li>
            <li><a href="/ulogout" ng-click="logout()"><small>Log Out</small></a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>
<div class="container">
  <div class="row" !ng-cloak ng-hide="true">
  <br><br>
  <div class="alert alert-danger">
  <center><strong>
  Something went wrong, if you are seeing this frequently please contact system administrator</strong></center></div></div>  
  <div class="row">
    <div>
        <toaster-container toaster-options="{'time-out': 3000, 'close-button':true, 'animation-class': 'toast-top-center'}"></toaster-container>
     </div>
     <!-- <div id="view" ng-view ng-cloak> </div> -->
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
     <div ui-view ng-cloak></div>  
      <br/>
      <br/>
  </div>
 </div> 
 <div style="position: fixed; right:0; bottom: 30px;">
 <a target="_blank" href="https://www.positivessl.com" style="font-family: arial; font-size: 10px; color: #212121; text-decoration: none;"><img src="https://www.positivessl.com/images-new/PositiveSSL_tl_trans2.png" alt="SSL Certificate" title="SSL Certificate" border="0" /></a>
 </div>
<div class="footer navbar-fixed-bottom">
      
      <div style="background:white">
      <span >myPay &#169;2014-2015 <a target="_blank" href="http://www.corientbs.com" target="_blank">
                  Corient Business Solutions Ltd. </a>All rights 
                reserved.</span>
  <span class="pull-right"><A href="/privacy" target="_blank">
                Privacy</A> | <A href="/tandc" target="_blank">
                Terms and Conditions</A>
                <!--  | <A href="/contactus" target="_blank">
                Contact Us</A> -->
                </span>
</div>
    </div>
</body>
</html>
</base>