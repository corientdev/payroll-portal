<!doctype html>
<html lang="en" ng-app="app">
<head>
  <meta charset="UTF-8">
  <title>Welcome to Payroll Portal</title>
  <!-- <link rel="stylesheet" href="packages/components/foundation/css/foundation.min.css"> -->
  <!-- <script type="text/javascript" src="public/packages/bower/headjs/dist/1.0.0/head.min.js" data-headjs-load="js/boot.js"></script> -->
  <style type="text/css">
  input[type=text], input[type=password], input[type=email], input[type=tel] {
    height: 32px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="public/packages/bower/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="public/packages/bower/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css">
  <!-- <link rel="stylesheet" href="packages/bower/normalize.css/normalize.css"> -->
  
  <script type="text/javascript" src="public/packages/bower/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="public/packages/bower/angularjs/angular.min.js"></script>
  <script type="text/javascript" src="public/packages/bower/angular-sanitize/angular-sanitize.min.js"></script>
  <script type="text/javascript" src="public/packages/bower/bootstrap/dist/js/bootstrap.js"></script>
  <script type="text/javascript" src="public/packages/bower/angular-bootstrap/ui-bootstrap.js"></script>
  <script type="text/javascript" src="public/packages/bower/angular-fileupload/angular-file-upload.min.js"></script>
  <!-- <script type="text/javascript" src="public/packages/bower/angular-route/angular-route.js"></script> -->
  <script type="text/javascript" src="public/packages/bower/angular-ui-router/release/angular-ui-router.min.js"></script>
  <script type="text/javascript" src="public/packages/bower/underscore/underscore-min.js"></script>
  <script type="text/javascript" src="public/packages/bower/angular-smart-table/dist/smart-table.min.js"></script>
  <script type="text/javascript" src="public/packages/bower/angular-validator/angular-validator.js"></script>

  <link rel="stylesheet" type="text/css" href="public/css/app.css">
  <script type="text/javascript" src="public/js/app.js"></script>
  <script type="text/javascript" src="public/js/appConfig.js"></script>
  <script type="text/javascript" src="public/js/controllers/BooksController.js"></script>
  <script type="text/javascript" src="public/js/controllers/FileuploadController.js"></script>
  <script type="text/javascript" src="public/js/controllers/ViewPayslipController.js"></script>
  <script type="text/javascript" src="public/js/controllers/viewclientController.js"></script>
  <script type="text/javascript" src="public/js/controllers/HomeController.js"></script>
  <script type="text/javascript" src="public/js/controllers/LoginController.js"></script>
  <script type="text/javascript" src="public/js/controllers/SignupController.js"></script>
  <script type="text/javascript" src="public/js/controllers/baseController.js"></script>
  <script type="text/javascript" src="public/js/controllers/UsersController.js"></script>
  <script type="text/javascript" src="public/js/directives/showsMessageWhenHovered.js"></script>
  <script type="text/javascript" src="public/js/directives/ManageModal.js"></script>
  <script type="text/javascript" src="public/js/services/AuthenticationService.js"></script>
  <script type="text/javascript" src="public/js/services/BookService.js"></script>
  <script type="text/javascript" src="public/js/services/FlashService.js"></script>
  <script type="text/javascript" src="public/js/services/SessionService.js"></script>

<!-- Adapt Strap -->
<script src="public/packages/bower/adapt-strap/adapt-strap.min.js"></script>
<script src="public/packages/bower/adapt-strap/adapt-strap.tpl.min.js"></script>
<link rel="stylesheet" href="packages/bower/adapt-strap/adapt-strap.min.css"/>
<!-- Adapt Strap -->

<script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>

  <script>
  angular.module("app").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
  </script>
</head>
<body ng-controller="baseController">
<nav class="navbar navbar-default" role="navigation" style="background-color:white; border:none;" ng-cloak>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand" href="#">
        Payroll Portal
      </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"  >
      <ul class="nav navbar-nav navbar-right" >
        <li><a class="" href="#"> Home</a></li>
        <!-- <li><a class="" href="#/contact"> Contact</a></li>
        <li><a class="" href="#/about"> About</a></li> -->
        <li class="dropdown">
          <a class="" data-toggle="dropdown" href="" ng-show = "user.name.length>0">Actions<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ng-repeat="menu in user.routes">
                <a href="#{{ menu.eroute }}">{{ menu.name }}</a>
            </li>
          </ul>
        </li>
        <li><a class="" href="#/login" ng-hide = "user.name.length>0"> Log In</a></li>
        <li class="dropdown" ng-show = "user.name.length>0">
          <a class="" href=""  data-toggle="dropdown"> {{ user.name }}<small>({{user.role}})</small> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#/cngpass">Change Password</a></li>
            <li><a href="#" ng-click="logout()">Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->

  </div><!-- /.container-fluid -->
</nav>
<div class="container">
  <div class="row" !ng-cloak ng-hide="true">
  <br><br>
  <div class="alert alert-danger">
  <center><strong>
  Something went wrong, if you are seeing this frequently please contact system administrator</strong></center></div></div>  
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
          <div id="flash" class="alert alert-danger" ng-show="flash" role="alert" ng-cloak>
            {{ flash }}
          </div>
     </div>
     <div id="view" ng-view ng-cloak> </div>
     <div ui-view ng-cloak></div>  
  </div>
 </div> 

</body>
</html>