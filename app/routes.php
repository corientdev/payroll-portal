<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function(){
	return View::make('index');
});

// Route::get('users', function(){
// 	return 'Users!';
// });


Route::group(array('before' => array('auth|roles:UL1')), function()
{
	Route::post('/uploadpslip', 'FileuploadController@uploadpslip');	
	Route::post('/uploadp32', 'FileuploadController@uploadp32');	
	Route::post('/uploadp60', 'FileuploadController@uploadp60');	
	Route::post('/getcompany', 'FileuploadController@getcompany');	
	Route::post('/getyears', 'FileuploadController@getyears');	
	Route::post('/signup/client', 'SignupController@signupclient');	
	Route::post('/updates/client', 'SignupController@updateclient');
	
	

});


Route::group(array('before' => array('auth|roles:UL1;CL1;')), function()
{
	Route::post('/viewclient', 'ViewClientController@viewclient');		
	Route::post('/updateclientstatus', 'ViewClientController@updateclientstatus');	
	Route::post('/resetpass', 'ViewClientController@resetpass');	
});

Route::group(array('before' => array('auth|roles:UL1;CL1;CL2')), function()
{
	Route::post('/ChngPass', 'UsersController@ChngPass');	
});

Route::group(array('before' => array('auth|roles:CL1;CL2')), function()
{
		//Route::post('/getemployees', 'UsersController@getemployees');	
		//Route::post('/p60employees', 'UsersController@p60employees');	
		Route::post('/viewpayslip', 'ViewPaySlipController@viewpayslip');	
		Route::post('/viewp60', 'ViewPaySlipController@viewp60');	
		Route::post('/viewp32', 'ViewPaySlipController@viewp32');	
		Route::post('/dwnldpayslip', 'ViewPaySlipController@dwnldpayslip');		
		Route::post('/dwnldp60', 'ViewPaySlipController@dwnldp60');		
		Route::post('/dwnldp32', 'ViewPaySlipController@dwnldp32');		
});

Route::group(array('before' => array('auth|roles:CL1')), function()
{
		Route::post('/getemployees', 'UsersController@getemployees');	
		Route::post('/p60employees', 'UsersController@p60employees');	
		Route::post('/getpayruns', 'ViewPaySlipController@getpayruns');
		//Route::post('/viewpayslip', 'ViewPaySlipController@viewpayslip');	
		//Route::post('/viewp60', 'ViewPaySlipController@viewp60');	
		//Route::post('/viewp32', 'ViewPaySlipController@viewp32');	
		//Route::post('/dwnldpayslip', 'ViewPaySlipController@dwnldpayslip');		
		//Route::post('/dwnldp60', 'ViewPaySlipController@dwnldp60');		
		//Route::post('/dwnldp32', 'ViewPaySlipController@dwnldp32');		
});

Route::group(array('before' => 'csrf_header'), function(){
	Route::post('/auth/login', 'AuthController@login');	
	Route::post('/getsession','AuthController@getsession');
	Route::post('/userlogout', 'AuthController@logout');
	//Route::get('/auth/logout', 'AuthController@logout');
	Route::post('/auth/createuser', 'AuthController@signup');	
	Route::post('/access', array('before' => 'auth', 'uses' => 'AccessController@getaccess'));
	Route::get('/accesscode', array('before' => 'auth', 'uses' => 'AccessController@getcode'));		
	Route::post('/signup/user', 'SignupController@signupuser');	
	Route::post('/forgot_password', 'RemindersController@postRemind');
	Route::post('/reset_password', 'RemindersController@postReset');
	Route::post('/svcontactus', 'ContactController@contactus');
	
});

// Route::group(array('before' => 'auth'), function(){
		
		
// 		Route::post('/getusers', 'UsersController@getusers');	
// 		
// 		Route::post('/update/{id}', 'UsersController@update');	
		
		
		
		
// 	});

Route::when('/UL1/*', 'IsUL1');

Route::get('/code', 'AccessController@getcode1');	

Route::get('/expiry', function(){
	return Response::json(array('flash' => 'your session has expired please log in...'), 401); 
});

Route::get('/books', array('before' => 'auth', function() {
  return Response::json(array(
    array('title' => 'Great Expectations', 'author' => 'Dickens'),
    array('title' => 'Foundation', 'author' => 'Asimov'),
    array('title' => 'Treasure Island', 'author' => 'Stephenson')
  ));
}));

App::missing(function($exception)
{
return View::make('index'); // this should match your index route
});
?>