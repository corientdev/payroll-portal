app.factory('FlashService', function($rootScope, toaster){
  return {
	show: function(message,type,duration){
			toaster.clear();
			duration = duration || 3000;
			toaster.pop(type, "Message", message, duration);
		},
		clear: function(){
			toaster.clear();
		}
  };
});