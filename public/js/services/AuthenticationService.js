app.factory("AuthenticationService", function($http, $location, $localStorage, $sanitize, SessionService, FlashService, CSRF_TOKEN ) {
  var cacheSession = function(data){
SessionService.set('data', JSON.stringify(data));
    $http.defaults.headers.common['X-CSRF-TOKEN'] = data['code'];
    if(data.role == 'guest'){
      SessionService.set('authenticated', false);
      SessionService.set('isFirsttime', false);
    }else{
      SessionService.set('authenticated', true);
      SessionService.set('isFirsttime', data['firsttime']);
    }
    $localStorage.loggedin = true;
    $localStorage.ltime = new Date ();
  };
  var uncacheSession = function(){
    SessionService.unset('authenticated');
    SessionService.unset('data');
    SessionService.unset('isFirsttime');
    $http.defaults.headers.common['X-CSRF-TOKEN'] = null;
    if($localStorage.loggedin){
    $localStorage.loggedin = false;
    $localStorage.ltime = null;
    $localStorage.notconfirmed = null;
    }
    return true
  };

  var loginerr = function(response){
    FlashService.show(response.flash,'error');
  }

  var sanitizeCredentials = function(credentials){
    return{
      email: $sanitize(credentials.email),
      password: $sanitize(credentials.password),
      ccode: $sanitize(credentials.ccode),
      csrf_token: CSRF_TOKEN
    }
  }
  return {
    login: function(credentials) {
      var promise = $http.post('/auth/login', sanitizeCredentials(credentials));
      promise.success(function(data){
        cacheSession(data);
        if(data["role"] != 'UL1'){
        $localStorage.notconfirmed = true;
        }
      });
      promise.error(loginerr);
      return promise;
    },
    logout: function() {
      var promise = $http.post('/userlogout', { csrf_token: CSRF_TOKEN });
      promise.success(uncacheSession);
      promise.error(loginerr);
      return promise;
    },
    isLoggedIn: function(){
      return JSON.parse(SessionService.get('authenticated'));
      // var promise = $http.post('/getsession', {csrf_token: CSRF_TOKEN});
      // promise.success(function(){ return true});
      // promise.error(function(){return false});
    },
    isConfirmed: function(){
      var retval =true;
      //console.log($localStorage.notconfirmed);
      if($localStorage.notconfirmed){
        retval =false;
      }
      return retval;
      // var promise = $http.post('/getsession', {csrf_token: CSRF_TOKEN});
      // promise.success(function(){ return true});
      // promise.error(function(){return false});
    },
    isSessionValid: function(){
      if(JSON.parse(SessionService.get('authenticated'))){
      if(Math.floor((Math.abs(new Date() - new Date($localStorage.ltime))/1000)) < 600){
      $localStorage.ltime = new Date ();
      return true;
      }
      else
      {
      return false;
      }
    }else{
      return true;
    }
    },
    isFirstTime: function(){
      return JSON.parse(SessionService.get('isFirsttime'));
      // var promise = $http.post('/getsession', {csrf_token: CSRF_TOKEN});
      // promise.success(function(){ return true});
      // promise.error(function(){return false});
    },
    // destroySession: function(){
    //   var promise = $http.post('/userlogout' , { csrf_token: CSRF_TOKEN });
    //   promise.success(uncacheSession);
    //   return true;
    // },
    saveSession: function(){
      var promise = $http.post('/getsession', {csrf_token: CSRF_TOKEN})
      promise.success(function(data){
      cacheSession(data)
      if(data.firsttime == true){
        if($location.path() != '/uchngpass' && $location.path() != '/ulogout'){
        $location.path('/uchngpass');
      }
      }
      if ($localStorage.notconfirmed){
            if($location.path() != '/confirmwho' && $location.path() != '/ulogout'){
              $location.path('/confirmwho');
            }
          }
      });
      return promise;
      //return true;
    },
    userRole: function(){
      if(SessionService.get('data') == null)
      {
        return null;    
      }
    return JSON.parse(SessionService.get('data')).role;;
    }
  };
});