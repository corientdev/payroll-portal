
app.controller("ContactController", function($location, $http, $filter, $localStorage, $scope, $modal, $log, AuthenticationService, FlashService, CSRF_TOKEN) {
$scope.formData;
$scope.submitButtonDisabled = false;
$scope.submit = function(){
	$scope.submitButtonDisabled = true;
	$scope.formData.csrf_token = CSRF_TOKEN;
	$scope.formData.userid = AuthenticationService.userRole();
	var promise = $http.post('/svcontactus', $scope.formData);
        promise.success(function(data){
        	$scope.formData = {};
        	$scope.contactform.submitted = false;
        	$scope.contactform.$setPristine();
        	$scope.submitButtonDisabled = false;
            FlashService.show(data.flash,'success');    
        });
        promise.error(function(data){
        	$scope.formData = {};
        	$scope.contactform.submitted = false;
        	$scope.contactform.$setPristine();
        	$scope.submitButtonDisabled = false;
            FlashService.show(data.flash,'error');    
        });
}
});