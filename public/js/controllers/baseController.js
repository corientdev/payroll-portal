'use strict';

app.controller('baseController', function($http, $scope, $localStorage, $location, SessionService, AuthenticationService, FlashService, CSRF_TOKEN){
	// var cacheSession = function(data){
 //    	SessionService.set('authenticated', true);
 //    	SessionService.set('data', JSON.stringify(data));
 //      SessionService.set('isFirsttime', data['firsttime']);
 //    	$http.defaults.headers.common['X-CSRF-TOKEN'] = data['code'];
 //    	FlashService.clear();
 //  		};
	$scope.$watch(
		function(){
			return JSON.parse(SessionService.get('data'));},
		function(newval){$scope.user = newval;},true);
    $scope.lstorage = $localStorage;
    //$scope.lstorage.loggedin = true;
    $scope.$watch('lstorage.loggedin', function() {
      if(!$scope.lstorage.loggedin && AuthenticationService.isLoggedIn()){
        AuthenticationService.logout();
        $location.path('/home');  
      }
    });
	// $scope.test = function(){
	// 	$http.post('/getsession', {csrf_token: CSRF_TOKEN}).success(function(data){
 //  		cacheSession(data);
 //  		$location.path('/home');
 //  		});

	// 	// $scope.flas = CSRF_TOKEN;
	// }

$scope.ChngPass = function(){
  $http.post('/ChngPass',{id : $scope.user.id, oldpass : $scope.cpass, newpass : $scope.npass, })
  .success(function(data){
    $scope.cpass="";
    $scope.npass="";
    $scope.cnpass="";
    $scope.SignupForm.submitted = false;
    $scope.SignupForm.$setPristine();
    SessionService.unset('authenticated');
    SessionService.unset('data');
    SessionService.unset('isFirsttime');
    $http.defaults.headers.common['X-CSRF-TOKEN'] = null;

    // var promisez = $http.post('/getsession', {csrf_token: CSRF_TOKEN});
    // promisez.success(function(data){
    //   SessionService.set('authenticated', true);
    //   SessionService.set('data', JSON.stringify(data));
    //   SessionService.set('isFirsttime', data['firsttime']);
    //   $http.defaults.headers.common['X-CSRF-TOKEN'] = data['code'];  
    //   });
      AuthenticationService.saveSession().success(function(){
      FlashService.show(data.flash,'success');
      $location.path('/home');  
      });
      
      
  }).error(function(data){
    $scope.cpass="";
    $scope.npass="";
    $scope.cnpass="";
    $scope.SignupForm.submitted = false;
    $scope.SignupForm.$setPristine();
    FlashService.show(data.flash,'error');

  });
};

// $scope.logout = function() {
//   AuthenticationService.logout().success(function(){
//   $location.path('/home');  
//   //if($scope.lstorage.loggedin){
//   FlashService.show("Logged Out Successfully...",'success');
//   //$scope.lstorage.loggedin = false;
//   //}
//   });

// };
	
});
