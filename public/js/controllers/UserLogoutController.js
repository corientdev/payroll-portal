'use strict';

app.controller('LogoutController', function($http, $scope, $localStorage, $location, SessionService, AuthenticationService, FlashService, CSRF_TOKEN){
	var shwmsg = false;
	if(AuthenticationService.isSessionValid()){
		shwmsg = true;
	}
  AuthenticationService.logout().success(function(){
  $location.path('/home');  
  if(shwmsg){
  FlashService.show("Logged Out Successfully...",'success');
  }
  });
});