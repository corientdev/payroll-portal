app.controller("ForgotPassController", function($scope, $location, vcRecaptchaService, AuthenticationService, $http, CSRF_TOKEN, FlashService) {
    if(AuthenticationService.isLoggedIn()){
        $location.path('/home');
    }else{
$scope.sbmtbtn = false;
$scope.model = {
                    key: '6LeqFP8SAAAAABzF80ETlbEatHiLeEW_hRvW8Kuc'
                };
$scope.ForgotPass = function(rmail) {
    $scope.sbmtbtn = true;
    var valdata = vcRecaptchaService.data();
    var promise = $http.post('/forgot_password', { email : rmail, response: valdata['response'], challenge: valdata['challenge'], csrf_token: CSRF_TOKEN });

        promise.success(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.sbmtbtn = false;
        $scope.remail = '';
        FlashService.show(data.flash,'success');   
        $location.path('/login');
         });
        promise.error(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.sbmtbtn = false;
        $scope.remail = '';
        vcRecaptchaService.reload();  
        FlashService.show(data.flash,'error');    
        //alert(data);
        //cacheSession(data);
      });

    //$scope.rmail = '';
  };
}
});