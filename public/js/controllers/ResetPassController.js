app.controller("ResetPassController", function($scope, vcRecaptchaService, AuthenticationService, $stateParams, $http, $location, CSRF_TOKEN, FlashService) {
        if(AuthenticationService.isLoggedIn()){
        $location.path('/home');
    }else{
$scope.sbmtbtn = false;
$scope.model = {
                    key: '6LeqFP8SAAAAABzF80ETlbEatHiLeEW_hRvW8Kuc'
                };
$scope.ResetPass = function(rmail,npass,cnpass) {
    $scope.sbmtbtn = true;
    var valdata = vcRecaptchaService.data();
    var promise = $http.post('/reset_password', { email : rmail, password : npass, password_confirmation : cnpass, token : $stateParams.validatortoken, response: valdata['response'], challenge: valdata['challenge'], csrf_token: CSRF_TOKEN });
        promise.success(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.remail = '';
        $scope.npass = '';
        $scope.cnpass = '';
        //$scope.sbmtbtn = false;
        $location.path('/home');
        FlashService.show(data.flash,'success');   
        $scope.sbmtbtn = false;
         });
        promise.error(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.remail = '';
        $scope.npass = '';
        $scope.cnpass = '';
        FlashService.show(data.flash,'error');    
        $scope.sbmtbtn = false;
        vcRecaptchaService.reload();
        //alert(data);
        //cacheSession(data);
    
      });

    //$scope.rmail = '';
  };
}
});