app.controller("ViewClientController", function($http, $filter, $scope, $modal, $log, SessionService, FlashService) {
  // $scope.getpayslip={ userid: "", coid: ""};
  // $scope.getpayslip.userid = $scope.user.userid;
  // $scope.getpayslip.coid = $scope.user.coid;

$scope.loading = true;
var dataload = null;
$scope.populatelist = function(){
      var promise = $http.post('/viewclient');
      promise.success(function(data){
        dataload=data;
        $scope.getdata = data;
        $scope.loading = false;
      });   
}


$scope.ModalStyle = {
    'max-height': '71%',
    'overflow-y' : 'auto',
    'overflow-x' : 'hidden',
    'width': '80%',
    'margin-left':'-40%',
    'height':'auto'
};

$scope.signupclient=  function(itm) {
    //alert('hi');
    if(itm=='Edit Client'){
      var promise = $http.post('/updates/client', $scope.credentials);
      promise.success(function(data){
      $scope.ClsModal();
      $scope.populatelist();
      FlashService.show(data.flash,'success');    
        //cacheSession(data);
      });
      promise.error(function(data){
      $scope.ClsModal();
      $scope.populatelist();
      FlashService.show(data.flash,'error');    
        //cacheSession(data);
      });
    }
    else if(itm=='Add Client'){
    var promise = $http.post('/signup/client', $scope.credentials);
      promise.success(function(data){
        $scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'success'); 
        //cacheSession(data);
      });
      promise.error(function(data){
        $scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'error');  
        //cacheSession(data);
      });
      }
  }


$scope.DeactUser = function(itmid,itmstatus){
  if(itmstatus=="Activate")
  {
    itmstatus=1;
  }else
  {
    itmstatus=0;
  }
      var promise = $http.post('/updateclientstatus', {id: itmid, status: itmstatus});
      promise.success(function(data){
        $scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'success');  
      //$scope.credentials = data[0];
      }); 
      promise.error(function(data){
        $scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'error');  
      //$scope.credentials = data[0];
      }); 
    }

    $scope.ClsModal = function(){
      $scope.showModal = false;
    }

    $scope.ResetPass = function(itm){
      var promise = $http.post('/resetpass',{id: itm.id});
      promise.success(function(data){
        FlashService.show(data.flash,'sucess');  
      }); 
      promise.error(function(data){
        FlashService.show(data.flash,'error');  
      }); 
    }

$scope.EditCl = function(itm){
  $scope.SignupForm.submitted = false;
  $scope.SignupForm.$setPristine();
  $scope.FormTitleText = 'Edit Client';
  $scope.pfielddisabled = true;
  $scope.emaildisabled = true;
  if(itm.status==1) {

    $scope.deactstr = "Deactivate";
  }
  else  
  {
   $scope.deactstr = "Activate"; 
  }
  var promise = $http.post('/viewclient', {id: itm.id});
      promise.success(function(data){
      $scope.credentials = data[0];
      }); 
  
      $scope.showModal = true;
    }

    $scope.search = function(){
  //$scope.LoadMsg = 'Loading...';
  $scope.getdata = $filter('filter')(dataload, $scope.filterOnLocation);
  // if($scope.users.length == 0){
  // $scope.LoadMsg = 'Records Not Found';
  // }
}

$scope.filterOnLocation = function(record) {
  //alert(record.userid);
  if(record.userid.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else if (record.name.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else if (record.coid.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else {
    return false;
  }
  
  };

    $scope.AddCl = function(){
  $scope.credentials={}
  $scope.SignupForm.submitted = false;
  $scope.SignupForm.$setPristine();
  $scope.FormTitleText = 'Add Client';
  $scope.pfielddisabled = false;
  $scope.emaildisabled = false;
  //$scope.pwdvalregex='';
  $scope.showModal = true;
    }
$scope.populatelist();


    $scope.carsTableColumnDefinition = [
   
      {
        columnHeaderDisplayName: 'Company Id',
        displayProperty: 'coid'
      },
      {
        columnHeaderDisplayName: 'Company Name',
        displayProperty: 'coname'
      },
      {
        columnHeaderDisplayName: 'User Id / Email Address',
        displayProperty: 'userid'
      },
      {
        columnHeaderDisplayName: 'Name',
        displayProperty: 'name'
      }
      // ,
      // {
      //   columnHeaderDisplayName: 'Mail Id',
      //   displayProperty: 'email'
      // }
      // ,
      // {
      //   columnHeaderDisplayName: 'User Role',
      //   displayProperty: 'role'
      // }
      ,
      {
        columnHeaderDisplayName: 'Status',
        templateUrl: 'btnStatus'
      }
      ,
      {
        columnHeaderDisplayName: 'Reset',
        templateUrl: 'btnResetPass'
      }
      ,
       {
        columnHeaderDisplayName: 'Edit',
        templateUrl: 'btnclick'
      }
    ];

  });
