app.controller("EditEndUserController", function($http, $filter, $scope, $modal, $log, SessionService, FlashService) {
  // $scope.getpayslip={ userid: "", coid: ""};
  // $scope.getpayslip.userid = $scope.user.userid;
  // $scope.getpayslip.coid = $scope.user.coid;
$scope.loading = true;
$scope.populatelist = function(){
      var promise = $http.post('/viewclient');
      promise.success(function(data){
        $scope.rcvddatalist = data;
        $scope.getdata = data;
        $scope.loading = false;
      });   
}

$scope.search = function(){
  //$scope.LoadMsg = 'Loading...';
  $scope.getdata = $filter('filter')($scope.rcvddatalist, $scope.filterOnLocation);
  //$scope.loadMore();
  // if($scope.users.length == 0){
  // $scope.LoadMsg = 'Records Not Found';
  // }
}

$scope.filterOnLocation = function(record) {
  if(record.name.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else if (record.userid.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else if (record.email.toLowerCase().indexOf($scope.searchText.toLowerCase())>= 0){
    return true;
  } else {
    return false;
  }
  
  };



$scope.DeactUser = function(itmid,itmstatus){
  if(itmstatus==0)
  {
    itmstatus=1;
  }else
  {
    itmstatus=0;
  }
      var promise = $http.post('/updateclientstatus', {id: itmid, status: itmstatus});
      promise.success(function(data){
        //$scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'success');  
      //$scope.credentials = data[0];
      }); 
      promise.error(function(data){
        //$scope.ClsModal();
        $scope.populatelist();
        FlashService.show(data.flash,'error');  
      //$scope.credentials = data[0];
      }); 
    }

    $scope.ResetPass = function(itm){
      var promise = $http.post('/resetpass',{id: itm.id});
      promise.success(function(data){
        FlashService.show(data.flash,'sucess');  
      }); 
      promise.error(function(data){
        FlashService.show(data.flash,'error');  
      }); 
    }

$scope.populatelist();


    $scope.carsTableColumnDefinition = [
   
      // {
      //   columnHeaderDisplayName: 'Company Id',
      //   displayProperty: 'coid'
      // },
      // {
      //   columnHeaderDisplayName: 'Company Name',
      //   displayProperty: 'coname'
      // },
      {
        columnHeaderDisplayName: 'User Id',
        displayProperty: 'userid'
      },
      {
        columnHeaderDisplayName: 'Name',
        displayProperty: 'name'
      },
      {
        columnHeaderDisplayName: 'Email Address',
        displayProperty: 'email'
      }
      // ,
      // {
      //   columnHeaderDisplayName: 'Mail Id',
      //   displayProperty: 'email'
      // }
      // ,
      // {
      //   columnHeaderDisplayName: 'User Role',
      //   displayProperty: 'role'
      // }
      ,
      {
        columnHeaderDisplayName: 'Status',
        templateUrl: 'btnStatus'
      }
      ,
      {
        columnHeaderDisplayName: 'Reset Password',
        templateUrl: 'btnResetPass'
      }
      ,
       {
        columnHeaderDisplayName: 'Enable/Disable',
        templateUrl: 'btnclick'
      }
    ];

  });
