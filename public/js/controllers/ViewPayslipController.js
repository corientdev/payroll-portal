//app.controller('ViewPayslipController', ['$scope', function ($scope) {
app.controller("ViewPayslipController", function($http, $scope, $modal, $log, SessionService, FlashService) {
  // $scope.getpayslip={ userid: "", coid: ""};
  // $scope.getpayslip.userid = $scope.user.userid;
  // $scope.getpayslip.coid = $scope.user.coid;
  // $scope.loading = true;
$scope.drpdown = {};
$scope.populatelist = function(p60){
      if(p60){
var promise = $http.post('/viewp60', $scope.getpayslip);
      promise.success(function(data){
        $scope.getdata = data;
        $scope.loading = false;
      });
      }else
      {
      var promise = $http.post('/viewpayslip', $scope.getpayslip);
      promise.success(function(data){
        $scope.getdata = data;
        $scope.loading = false;
      });   
      }
}
$scope.selectchng = function(empchng){
  //alert($scope.user.coid);
  if(empchng){
    $scope.payruns = {};
    $scope.drpdown.payrun = '';
  var promise = $http.post('/getpayruns', {userid : $scope.drpdown.empidpay});
      promise.success(function(data){
        $scope.payruns = data;
        //cacheSession(data);
      });
  }
  $scope.loading = true;
  $scope.getdata = {};
  if(typeof $scope.drpdown.empidpay != 'undefined' && typeof $scope.drpdown.payrun != 'undefined'){
  if($scope.drpdown.empidpay.length>0 && $scope.drpdown.payrun.length>0){
  $scope.getpayslip.userid = $scope.drpdown.empidpay;
  $scope.getpayslip.coid = $scope.user.coid;
  $scope.getpayslip.payrun = $scope.drpdown.payrun;
  $scope.populatelist();
  }
}
}
$scope.selectchngp60 = function(){
  //alert($scope.user.coid);
  $scope.getpayslip.userid = $scope.drpdown.empidp60;
  $scope.getpayslip.coid = $scope.user.coid;
  $scope.populatelist(true);
}
$scope.selectedp60 = function(){
  $scope.drpdown = {};
  $scope.getpayslip={};
  $scope.payruns = {};
  $scope.emps = {};
  //$scope.getpayslip.userid = $scope.user.userid;
  //$scope.getpayslip.coid = $scope.user.coid;
  $scope.loading = true;
  $scope.getdata = {};
  if($scope.user.role=='CL1'){
  var promise = $http.post('/p60employees');
      promise.success(function(data){
        $scope.emps = data;
        //cacheSession(data);
      });
      //$scope.populatelist();
       $scope.showdrpdwn = true;
}
else
{
$scope.showdrpdwn = false;
$scope.getpayslip.userid = $scope.user.userid;
$scope.getpayslip.coid = $scope.user.coid;
$scope.populatelist(true);
}
}

$scope.selectedp32 = function(){
  $scope.loading = true;
  $scope.getdata = {};
  if($scope.user.role=='CL1'){
  var promise = $http.post('/viewp32');
      promise.success(function(data){
        $scope.getdata = data;
        $scope.loading = false;
      });
      //$scope.populatelist();
       
}
// else
// {
// $scope.showdrpdwn = false;
// $scope.populatelist(true);
// }
}
$scope.selectedpayslip = function(){
  //alert(1);
  $scope.drpdown = {};
  $scope.getpayslip= {};
  //$scope.getpayslip.userid = $scope.user.userid;
  //$scope.getpayslip.coid = $scope.user.coid;
  $scope.loading = true;
  $scope.emps = {};
  $scope.getdata = {};
  if($scope.user.role=='CL1'){
  var promise = $http.post('/getemployees');
      promise.success(function(data){
        $scope.emps = data;
        //cacheSession(data);
      });
      //$scope.populatelist();
      //$scope.populatelist();
       $scope.showdrpdwn = true;
}
else
{
$scope.showdrpdwn = false;
$scope.getpayslip.userid = $scope.user.userid;
$scope.getpayslip.coid = $scope.user.coid;
$scope.getpayslip.payrun = 'all';
$scope.populatelist();
}
}
//$scope.selectedpayslip();
//$scope.getdata='';
// if($scope.user.role=='CL1'){
// var promise = $http.post('/getemployees');
//       promise.success(function(data){
//         $scope.emps = data;
//         //cacheSession(data);
//       });
//       //$scope.populatelist();
//        $scope.showdrpdwn = true;
// }
// else
// {
// $scope.showdrpdwn = false;
// $scope.populatelist();
// }
  //$scope.abc=SessionService.get('data');
    // $scope.models = {
    //   changeInfo: [],
    //   // selectedCars: [
    //   //   {
    //   //     id: 1,
    //   //     name: 'Audi A4',
    //   //     modelYear: 2009,
    //   //     price: 34000
    //   //   }
    //   // ],
      
    // };

    $scope.paysTableColumnDefinition = [
    {
        columnHeaderDisplayName: 'View/Download',
        columnHeaderDisplayName: 'View',
        templateUrl: 'btnclickpay'
      },
      {
        columnHeaderDisplayName: 'Name',
        displayProperty: 'name'
      },
      {
        columnHeaderDisplayName: 'Pay Date',
        cellFilter: 'date:dd/MM/yyyy',
        displayProperty: 'Payslip_date'
      },
      {
        columnHeaderDisplayName: 'Gross Pay',
        displayProperty: 'Gross_Pay'
      },
      {
        columnHeaderDisplayName: 'Net Pay',
        displayProperty: 'Net_Pay'
      },
      {
        columnHeaderDisplayName: 'Deduction',
        displayProperty: 'Deductions'
      }
      ,
      {
        columnHeaderDisplayName: 'Payment Mode',
        displayProperty: 'Pay_Mode'
      }
      ,
      {
        columnHeaderDisplayName: 'Payment Period',
        displayProperty: 'Pay_Period'
      }
    ];

    $scope.p60TableColumnDefinition = [
    {
        columnHeaderDisplayName: 'View/Download',
        columnHeaderDisplayName: 'View',
        templateUrl: 'btnclickp60'
      },
      {
        columnHeaderDisplayName: 'Name',
        displayProperty: 'name'
      },
      {
        columnHeaderDisplayName: 'NI Number',
        displayProperty: 'ninumber'
      },
      {
        columnHeaderDisplayName: 'Year',
        displayProperty: 'year'
      },
      {
        columnHeaderDisplayName: 'Pay',
        displayProperty: 'pay'
      },
      {
        columnHeaderDisplayName: 'Tax Deducted',
        displayProperty: 'taxdeducted'
      }
    ];


$scope.p32TableColumnDefinition = [
    {
        columnHeaderDisplayName: 'View/Download',
        columnHeaderDisplayName: 'View',
        templateUrl: 'btnclickp32'
      },
      {
        columnHeaderDisplayName: 'Date From',
        cellFilter: 'date:dd/MM/yyyy',
        displayProperty: 'datefrom'
      },
      {
        columnHeaderDisplayName: 'Date To',
        cellFilter: 'date:dd/MM/yyyy',
        displayProperty: 'dateto'
      },
      {
        columnHeaderDisplayName: 'Tax Month From',
        displayProperty: 'tax_month_from'
      },
      {
        columnHeaderDisplayName: 'Tax Month To',
        displayProperty: 'tax_month_to'
      },
      {
        columnHeaderDisplayName: 'Net Income Tax',
        displayProperty: 'nettax'
      },
      {
        columnHeaderDisplayName: 'Net National Insurance',
        displayProperty: 'netni'
      },
      {
        columnHeaderDisplayName: 'Total Amount Due',
        displayProperty: 'amtdue'
      }
      //
      //
      //

    ];
    // ========== ui handlers ========== //



// $scope.items = ['item1', 'item2', 'item3'];
//   $scope.open = function (size) {
// alert(1);
//     var modalInstance = $modal.open({
//       templateUrl: 'myModalContent.html',
//       controller: 'ModalInstanceCtrl',
//       size: 'lg'
//       ,
//       resolve: {
//         items: function () {
//           return $scope.items;
//         }
//       }
//     });

//     // modalInstance.result.then(function (selectedItem) {
//     //   $scope.selected = selectedItem;
//     // }, function () {
//     //   $log.info('Modal dismissed at: ' + new Date());
//     // });
//   };
    $scope.DwPdf = function(){
      window.open($scope.PDFPath,'_blank');       
      //downloadme(itm);
    }
    $scope.ClsModal = function(){
      $scope.dismiss = "modal";
    }
    $scope.PSView = function (pay,download) {
      //alert(1);
      //$scope.usr = 1;
      $scope.dwpayslip={ id: "", userid: "", coid: "" };
      $scope.dwpayslip.id = pay.id;
      var uid;
      if($scope.user.role=='CL1'){
      uid=$scope.drpdown.empidpay;
      }
      else
      {
        uid=$scope.user.userid;
      }
      $scope.dwpayslip.userid = uid;
      $scope.dwpayslip.coid = $scope.user.coid;
      var promise = $http.post('/dwnldpayslip', $scope.dwpayslip);
      promise.success(function(data){
        if(download){
          window.open(data,'_blank');
          //downloadme(data);
        }
        else {
        $scope.PDFPath = data;
        }
        //cacheSession(data);
      });
      //$scope.PDFPath = '/pdfSample.pdf';
      // $scope.dismiss = "";
      // if (usr){ 
      //   $scope.usr = angular.copy(usr);
      // }
      //$scope.showModal=true;
      // var promise = $http.post('/dwnldpayslip', pay.id);
      // promise.success(function(data){
      //   $scope.pdflink = data;
      //   //cacheSession(data);
      // });
      //pay.id);
    };

    $scope.P60View = function (pay,download) {
      //alert(1);
      //$scope.usr = 1;
      $scope.dwpayslip={ id: "", userid: "", coid: "" };
      $scope.dwpayslip.id = pay.id;
      var uid;
      if($scope.user.role=='CL1'){
      uid=$scope.drpdown.empidp60;
      }
      else
      {
        uid=$scope.user.userid;
      }
      $scope.dwpayslip.userid = uid;
      $scope.dwpayslip.coid = $scope.user.coid;
      var promise = $http.post('/dwnldp60', $scope.dwpayslip);
      promise.success(function(data){
        if(download){
          window.open(data,'_blank');
          //downloadme(data);
        }
        else {
        $scope.PDFPath = data;
        }
        //cacheSession(data);
      });
      //$scope.PDFPath = '/pdfSample.pdf';
      // $scope.dismiss = "";
      // if (usr){ 
      //   $scope.usr = angular.copy(usr);
      // }
      //$scope.showModal=true;
      // var promise = $http.post('/dwnldpayslip', pay.id);
      // promise.success(function(data){
      //   $scope.pdflink = data;
      //   //cacheSession(data);
      // });
      //pay.id);
    };


    $scope.P32View = function (pay,download) {
      //alert(1);
      //$scope.usr = 1;
      $scope.dwpayslip={ id: pay.id};
      var promise = $http.post('/dwnldp32', $scope.dwpayslip);
      promise.success(function(data){
        if(download){
          window.open(data,'_blank');
          //downloadme(data);
        }
        else {
        $scope.PDFPath = data;
        }
        //cacheSession(data);
      });
      //$scope.PDFPath = '/pdfSample.pdf';
      // $scope.dismiss = "";
      // if (usr){ 
      //   $scope.usr = angular.copy(usr);
      // }
      //$scope.showModal=true;
      // var promise = $http.post('/dwnldpayslip', pay.id);
      // promise.success(function(data){
      //   $scope.pdflink = data;
      //   //cacheSession(data);
      // });
      //pay.id);
    };
  });

// app.controller('ModalDemoCtrl', function ($scope, $modal, $log) {

//   $scope.items = ['item1', 'item2', 'item3'];

//   $scope.open = function (size) {

//     var modalInstance = $modal.open({
//       templateUrl: 'myModalContent.html',
//       controller: 'ModalInstanceCtrl',
//       size: size,
//       resolve: {
//         items: function () {
//           return $scope.items;
//         }
//       }
//     });

//     modalInstance.result.then(function (selectedItem) {
//       $scope.selected = selectedItem;
//     }, function () {
//       $log.info('Modal dismissed at: ' + new Date());
//     });
//   };
// })

// app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

//   $scope.items = items;
//   $scope.selected = {
//     item: $scope.items[0]
//   };

//   $scope.ok = function () {
//     $modalInstance.close($scope.selected.item);
//   };

//   $scope.cancel = function () {
//     $modalInstance.dismiss('cancel');
//   };
// });
