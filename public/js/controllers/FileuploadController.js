//app.controller('FileuploadController', ['$scope', 'FileUploader', function($scope, FileUploader, FlashService) {
app.controller('FileuploadController', function($scope, $http, FileUploader, $location, FlashService){
    if($location.path() == '/fileuploadp60'){
        var promise = $http.post('/getcompany');
        promise.success(function(data){
            $scope.cmpnames = data;
        });
        var promise = $http.post('/getyears');

        promise.success(function(data){
            $scope.upyears = data;
        });
    }
        var uploader = $scope.uploader = new FileUploader({
            queueLimit: '2'
        });

        // FILTERS

        // uploader.filters.push({
        //     name: 'customFilter',
        //     fn: function(item /*{File|FileLikeObject}*/, options) {
        //         return this.queue.length < 10;
        //     }
        // });
        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|pdf|xml|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            $scope.uploading = true;
            switch($location.path()) {
    case '/upldpayslip':
        item.url = '/uploadpslip';
        break;
    case '/fileuploadp32':
        item.url = '/uploadp32';
        break;
        case '/fileuploadp60':
        item.url = '/uploadp60';
        break;
    } 
            
            if(item.file.name==uploader.queue[0].file.name){
                var rand=Math.random();
                $scope.uniquekey=rand;
                if($location.path() == '/fileuploadp60'){
                formData = [{
                uniquekey: $scope.uniquekey,
                Final: 'false',
                year: $scope.upyear,
                coname: $scope.cmpname,
                }];
                }else
                {
                formData = [{
                uniquekey: $scope.uniquekey,
                Final: 'false',
                }];
                }
                Array.prototype.push.apply(item.formData, formData);
                //$scope.uniquekey="";
            }

            if(item.file.name==uploader.queue[uploader.queue.length-1].file.name){
                if($location.path() == '/fileuploadp60'){
                formData = [{
                uniquekey: $scope.uniquekey,
                Final: 'true',
                year: $scope.upyear,
                coname: $scope.cmpname,
                }];
            }else{
                formData = [{
                uniquekey: $scope.uniquekey,
                Final: 'true',
                }];
            }
                Array.prototype.push.apply(item.formData, formData);
                $scope.uniquekey="";
            }

            if(uploader.queue.length!=2){
                uploader.cancelAll();
                uploader.clearQueue();
                //alert("Only one XML and one PDF is allowed.")
                FlashService.show('Only one XML and one PDF is allowed.','error');
            }
            var ii;
            var xmldone=false;
            var pdfdone=false;
            for(ii=0;ii<uploader.queue.length;ii++)
            {
                if (getExt(uploader.queue[ii].file.name)=='xml')
                {
                    xmldone=true;
                }
                else
                {
                    pdfdone=true;   
                }
            }
            if ((xmldone==false) || (pdfdone==false))
            {
                uploader.cancelAll();
                uploader.clearQueue();
                FlashService.show('Only one XML and one PDF is allowed.','error');
            }
                //alert(ii.file.name);                          
            //}
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
            uploader.onCompleteItem = function(fileItem, response, status, headers) {
                $scope.uploading = false;
        if(status == 200){
            if($location.path() == '/fileuploadp60'){
            $scope.cmpname = '';
            $scope.upyear = '';
            $scope.SignupForm.submitted = false;
            $scope.SignupForm.$setPristine();
        }
            FlashService.show(response.flash,'success');    
            setTimeout(function () {
            uploader.clearQueue();
            }, 5000);
        }
        else if(status == 400)
        {
            FlashService.show(response.flash,'error'); 
            setTimeout(function () {
            uploader.clearQueue();
            }, 5000);   
        }
        
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
    });
function getExt(filename)
{
    var a = filename.split(".");
    if( a.length === 1 || ( a[0] === "" && a.length === 2 ) ) 
    {
        return "";
    }
    return a.pop();
}