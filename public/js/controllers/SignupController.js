app.controller("SignupController", function($http, $scope, $sanitize, $location, $state, AuthenticationService, SessionService, CSRF_TOKEN, FlashService) {
  if ($location.path()=='/createuser'){
    if(AuthenticationService.isLoggedIn()){
          $location.path('/home');
    }
    else
    {
      $scope.credentials = {};
    }
  }
  // else if ($location.path()=='/createclient'){
  // {
  //   //alert('hi');
  //   if(!AuthenticationService.isLoggedIn()){
  //         $location.path('/login');
  //   }
  //  $scope.credentials = { email: "", password: "", fname: "", 
  //                         coname: "", address: "", pcode: "", cnumber: ""};
  // }

//}
 // $scope.signupclient=  function() {
 //    //alert('hi');
 //    var promise = $http.post('/signup/client', $scope.credentials);
 //      promise.success(function(data){
 //        alert(data);
 //        //cacheSession(data);
 //      });
 //  }

  $scope.signupuser=  function() {
    //alert('hi');
    var promise = $http.post('/signup/user', sanitizeCredentials($scope.credentials));
      promise.success(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.credentials = {};
        FlashService.show(data.flash,'success');   
         });
        promise.error(function(data){
        $scope.SignupForm.submitted = false;
        $scope.SignupForm.$setPristine();
        $scope.credentials = {};
        FlashService.show(data.flash,'error');    
        //alert(data);
        //cacheSession(data);
      });
  }

$scope.ResetFrm=  function() {
  $scope.credentials = {};
    $scope.SignupForm.submitted = false;
    $scope.SignupForm.$setPristine();
    // var promise = $http.post('/signup/user', sanitizeCredentials($scope.credentials));
    //   promise.success(function(data){
    //     //cacheSession(data);
    //   });
  }

  var sanitizeCredentials = function(credentials){
    //alert('in');
    return{
      ccode: $sanitize(credentials.ccode),
      empref: $sanitize(credentials.empref),
      ninumber: $sanitize(credentials.ninumber),
      lsalary: $sanitize(credentials.lsalary),
      password: $sanitize(credentials.password),
      fname: $sanitize(credentials.fname),
      address: $sanitize(credentials.address),
      pcode: $sanitize(credentials.pcode),
      email: $sanitize(credentials.email),
      cnumber: $sanitize(credentials.cnumber),
      csrf_token: CSRF_TOKEN
    }
  }
  //$scope.credentials = { ninumber: "", coname: "", fname: "", lname: "", uname: "", email: "", password: ""};
  //SessionService.unset('authenticated');
  //SessionService.unset('data');
  // $scope.signup = function() {
  //   AuthenticationService.login($scope.credentials).success(function(){
  //     $location.path('/home');
  //   });
  // }

});