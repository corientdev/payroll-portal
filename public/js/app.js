var app = angular.module("app", ['ngAnimate', 'ngStorage', 'vcRecaptcha', 'toaster', 'angularValidator', 'ui.bootstrap', 'adaptv.adaptStrap', 'ngSanitize', 'angularFileUpload', 'ui.router', 'smart-table', 'ui.bootstrap', 'permission']);

app.run(function($rootScope, $location, $state, $localStorage, $http, $q, Permission, AuthenticationService, FlashService, SessionService, CSRF_TOKEN){
AuthenticationService.saveSession().success(function(){
$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
    if(typeof toState.url != 'undefined'){
      if(AuthenticationService.isSessionValid()){
        if(AuthenticationService.isLoggedIn() && $localStorage.loggedin){
          //console.log(1);
          if (AuthenticationService.isConfirmed() == false){
            if(toState.url != '/confirmwho' && toState.url != '/ulogout'){
              window.location.assign('/confirmwho');
            }
          }else if (AuthenticationService.isFirstTime() == true){
            if(toState.url != '/uchngpass' && toState.url != '/ulogout'){
              window.location.assign('/uchngpass');
            }
          }
        }
      }else
      {
        AuthenticationService.logout().success(function(){
          $location.path('/home');  
          FlashService.show('Session Expired...','error');
        });
      }
    }
  });
  });  

Permission.defineRole('guest', function (stateParams) {
    return !AuthenticationService.isLoggedIn();
    });

Permission.defineRole('UL1', function (stateParams) {
  // if(AuthenticationService.userRole() == 'UL1'){
  //   //alert(2);
  //   return true;
  // }
  var deferred = $q.defer();
  if(AuthenticationService.userRole() == 'UL1'){
    return true
  }
  else if(AuthenticationService.userRole() == null){
  $http.post('/getsession', {csrf_token: CSRF_TOKEN}).then(function (resp) {
      // if(resp.data.role != "guest"){
      //     AuthenticationService.saveSession(resp.data);
      //     }else{
      //       AuthenticationService.destroySession();
      //     }
          if (resp.data['role'] == 'UL1') {
              deferred.resolve();
            } else {
              deferred.reject();
            }
          }, function () {
            deferred.reject();
          });

          return deferred.promise;
          }
});

Permission.defineRole('CL1', function (stateParams) {
  // if(AuthenticationService.userRole() == 'UL2'){
  //   //alert(3);
  //   return true;
  // }
  var deferred = $q.defer();
if(AuthenticationService.userRole() == 'CL1'){
    return true
  }
  else if(AuthenticationService.userRole() == null){
  $http.post('/getsession', {csrf_token: CSRF_TOKEN}).then(function (resp) {
      // if(resp.data.role != "guest"){
      //     AuthenticationService.saveSession(resp.data);
      //     }else{
      //       AuthenticationService.destroySession();
      //     }
          if (resp.data['role'] == 'CL1') {
              deferred.resolve();
            } else {
              deferred.reject();
            }
          }, function () {
            deferred.reject();
          });

          return deferred.promise;
        }
});

Permission.defineRole('CL2', function (stateParams) {
  //alert(4);
 // if(AuthenticationService.userRole() == 'CL1'){
 //    return true;
 // }


 var deferred = $q.defer();
if(AuthenticationService.userRole() == 'CL2'){
    return true
  }
  else if(AuthenticationService.userRole() == null){
  $http.post('/getsession', {csrf_token: CSRF_TOKEN}).then(function (resp) {
      // if(resp.data.role != "guest"){
      //     AuthenticationService.saveSession(resp.data);
      //     }else{
      //       AuthenticationService.destroySession();
      //     }
          if (resp.data['role'] == 'CL2') {
              deferred.resolve();
            } else {
              deferred.reject();
            }
          }, function () {
            deferred.reject();
          });

          return deferred.promise;
        }
});

});

  



