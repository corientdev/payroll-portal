app.config(function($stateProvider, $urlRouterProvider, $locationProvider){
  $urlRouterProvider.otherwise("/404");
  $locationProvider.html5Mode(true);
  $stateProvider
    .state('login', {
      url:"/login",
      templateUrl: 'templates/login.html',
      controller: 'LoginController',
    data: {
        permissions: {
          only: ['guest'],
          redirectTo: 'home'
        }
      }
    })
    .state('uchngpass', {
    url:"/uchngpass",
    templateUrl: 'templates/chngpass.html',
    controller: 'baseController',
    data: {
        permissions: {
          only: ['UL1', 'CL1', 'CL2'],
          redirectTo: '404'
        }
      }
  })
      .state('contactus', {
    url:"/contactus",
    templateUrl: 'templates/contact.html',
    controller: 'ContactController',
    data: {
        permissions: {
          only: ['UL1', 'CL1', 'CL2','guest'],
          redirectTo: '404'
        }
      }
  })
    .state('ulogout', {
    url:"/ulogout",
    //templateUrl: 'templates/home.html',
    controller: 'LogoutController',
    data: {
        permissions: {
          only: ['UL1', 'CL1', 'CL2'],
          redirectTo: '404'
        }
      }
  })
  .state('forgotpass', {
    url:"/forgotpass",
    templateUrl: 'templates/forgotpassword.html',
    controller: 'ForgotPassController',
    data: {
        permissions: {
          only: ['guest'],
          redirectTo: 'home'
        }
      }
  })
  .state('home',{
      url:'/home',
      templateUrl: 'templates/home.html'
    })
  .state('Main',{
      url:'/',
      templateUrl: 'templates/home.html'
    })
    .state('upldpayslip',{
      url:'/upldpayslip',
      templateUrl: 'templates/upldpayslip.html',
      controller: 'FileuploadController',
    data: {
        permissions: {
          only: ['UL1'],
          redirectTo: '404'
        }
      }
    })
    .state('fileuploadp32',{
      url:'/fileuploadp32',
      templateUrl: 'templates/upldp32.html',
      controller: 'FileuploadController',
    data: {
        permissions: {
          only: ['UL1'],
          redirectTo: '404'
        }
      }
    })
    .state('fileuploadp60',{
      url:'/fileuploadp60',
      templateUrl: 'templates/upldp60.html',
      controller: 'FileuploadController',
    data: {
        permissions: {
          only: ['UL1'],
          redirectTo: '404'
        }
      }
    })
    .state('tandc',{
      url:'/tandc',
      //templateUrl: 'templates/confirmwho.html',
      templateUrl: 'templates/tandc.html',
    })
    .state('privacy',{
      url:'/privacy',
      templateUrl: 'templates/privacy.html',
    })

    .state('confirmwho', {
    url:"/confirmwho",
    templateUrl: 'templates/confirmwho.html',
    controller: 'ConfirmWhoController',
    data: {
        permissions: {
          only: ['CL1', 'CL2'],
          redirectTo: 'home'
        }
      }
  })
    .state('viewepayslip',{
      url:'/viewepayslip',
      templateUrl: 'templates/viewpayslip.html',
      controller: 'ViewPayslipController',
    data: {
        permissions: {
          only: ['CL1', 'CL2'],
          redirectTo: '404'
        }
      }
    })
    .state('createclient',{
      url:'/createclient',
      templateUrl: 'templates/createclient.html',
      controller: 'SignupController',
    data: {
        permissions: {
          only: ['UL1'],
          redirectTo: '404'
        }
      }
    })
    .state('vieweclient',{
      url:'/vieweclient',
      templateUrl: 'templates/viewclient.html',
      controller: 'ViewClientController',
    data: {
        permissions: {
          only: ['UL1'],
          redirectTo: '404'
        }
      }
    })
    .state('vieweusers',{
      url:'/vieweusers',
      templateUrl: 'templates/editenduser.html',
      controller: 'EditEndUserController',
    data: {
        permissions: {
          only: ['CL1'],
          redirectTo: '404'
        }
      }
    })
    .state('createuser',{
      url:'/createuser',
      templateUrl: 'templates/createuser.html',
      controller: 'SignupController',
    data: {
        permissions: {
          only: ['guest'],
          redirectTo: '404'
        }
      }
    })
    .state('passwordreset', {
    url:"/password/reset/:validatortoken",
    templateUrl: 'templates/resetpassword.html',
    controller: 'ResetPassController',
    data: {
        permissions: {
          only: ['guest'],
          redirectTo: 'home'
        }
      }
    })
    .state('404',{
    url:'/404',
    templateUrl: 'templates/404.html'
    })
    .state('main',{
    url:'/',
    templateUrl: 'templates/home.html'
  })
    // .state('users',{
    //   url:'/users',
    //   templateUrl: 'templates/users.html',
    //   controller: 'UsersController'
    // })
    // .state('users.add',{
    //   url:'/users/add',
    //   templateUrl: 'templates/add_edit_users.html',
    //   controller: 'UsersController'
    // })
  ;
});

app.config(function($httpProvider){
  var logsOutUseron401 = function($location, $localStorage, $q, SessionService, FlashService){
    var success = function(response){
      if(typeof response.data.flash != 'undefined'){
      //FlashService.show(response.data.flash,'success');  
      }
      return response;
    };
    var error = function(response){
      if (response.status === 401 ){
        // if(SessionService.get('authenticated') == 'true'){
          
        //   //$scope.lstorage.loggedin = false;
        // }
        if($localStorage.loggedin){
          FlashService.show(response.data.flash,'error');    
          $localStorage.loggedin = false;
        }
        SessionService.unset('authenticated');
        SessionService.unset('data');
        SessionService.unset('isFirsttime');
        $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = null;
        $location.path('/404');
      } else {
        if(typeof response.data.flash === 'undefined'){
          if(response.status != 404){
            FlashService.show("You met with an error. Please try again.",'error');  
          }
        
      }else{
        //FlashService.show(response.data.flash,'error');  
      }
      }
      return $q.reject(response);
    };

    return function(promise){
      return promise.then(success, error);
    };
  };
  $httpProvider.responseInterceptors.push(logsOutUseron401);
});

// app.config(function($httpProvider){
//   var logsOutUseron401 = function($location, $q, SessionService, FlashService){
//     var success = function(response){
//       return response;
//     };
//     var error = function(response){
//       if (response.status === 401 ){
//         SessionService.unset('authenticated');
//         $location.path('/login');
//         FlashService.show(response.data.flash,'error');  
//       } 
//         return $q.reject(response);
//     };

//     return function(promise){
//       return promise.then(success, error);
//     };
//   };
//   $httpProvider.responseInterceptors.push(logsOutUseron401);
// });

// app.config(function($httpProvider, CSRF_TOKEN) {
//   var user = JSON.parse(sessionStorage.getItem('data'));
//   if (user != null){
//     $httpProvider.defaults.headers.common['X-CSRF-TOKEN']  = user.code;  
//   } else {
//     $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = "";  
//   }
//   // $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = CSRF_TOKEN;  
// });